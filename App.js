import React, { Component } from 'react';
import { StyleSheet,AppRegistry, Platform, Text, View, Image } from 'react-native';

import {
  createSwitchNavigator, createAppContainer, createBottomTabNavigator, createStackNavigator
} from 'react-navigation'
import { Ionicons } from '@expo/vector-icons';
import Explore from './screens/Explore';
import Home from './screens/Home';
import Notification from './screens/Notification'
import Profile from './screens/Profile'
import LoadingScreen from './screens/LoadingScreen'
import SignupScreen from './screens/SignupScreen'
import ManageInterest from './screens/ManageInterest'
import * as firebase from 'firebase'
import { firebaseConfig } from './config'
import {name as appName} from './app.json';
firebase.initializeApp(firebaseConfig);
import Icon from 'react-native-vector-icons/Ionicons'
AppRegistry.registerComponent(appName, () => App);
class App extends React.Component {
  render() {
    if (Platform.OS === 'web') {
      AppRegistry.runApplication(appName, {
          rootTag: document.getElementById('root'),
      });
 }
    return (
      <View>
        <AppContainer />
      </View>
    );
  }
}

const AppBottomNavigator = createBottomTabNavigator({
  Home: {
    screen: Home,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <Icon name="ios-book" size={24} color={tintColor} />
      ),
      
      title: 'Read',
    }),

  },
  Explore: {
    screen: Explore,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Icon name="ios-search" size={24} color={tintColor} />
      ),
      title: 'Explore',
    },
  },
  LiveLearn: {
    screen: ManageInterest,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Icon name="ios-aperture" size={24} color={tintColor} />
      ),
      title: 'Interest',
      tabBarVisible: false
    }
  },
  // Notification: {
  //   screen: Notification,
  //   navigationOptions: {
  //     tabBarIcon: ({ tintColor }) => (
  //       <Icon name="ios-notifications" size={24} color={tintColor} />
  //       // <Image source={require('./assets/iconAdd.png')} style={{ height: 24, width: 24, tintColor: tintColor }} />
  //     )
  //   }
  // },
  Profile: {
    screen: Profile,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Icon name="ios-analytics" size={24} color={tintColor} />
      )
    }
  },
}
  , {
    tabBarOptions: {

      activeTintColor: 'black',
      inactiveTintColor: 'grey',
      animationEnabled: true,
      swipeEnabled: true,
      style: {
        backgroundColor: 'white',
        borderTopWidth: 0,
        shadowOffset: { width: 5, height: 3 },
        shadowColor: 'black',
        shadowOpacity: 0.5,
        elevation: 5,
        // display: 'none'
      }
    }

  },

  {
    defaultNavigationOptions: ({ navigation }) => ({

    }),

  }

)

const AppSwitchNavigator = createSwitchNavigator({
  LoadingScreen: LoadingScreen,
  SignupScreen: SignupScreen,
  Interest: ManageInterest,
  Home: AppBottomNavigator,
})

const AppContainer = createAppContainer(AppSwitchNavigator);
export default AppContainer

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
