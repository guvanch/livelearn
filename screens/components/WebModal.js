import React, { Component } from 'react';
import {
    AppRegistry, FlatList, StyleSheet, Text, View, Image, Alert,
    Platform, TouchableHighlight, Dimensions, ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import Modal from 'react-native-modalbox';
import { WebView } from 'react-native-webview'
import { Stopwatch } from 'react-native-stopwatch-timer'
import * as firebase from 'firebase';
var screen = Dimensions.get('window');
import { Header } from 'react-native-elements';
var userId;
export default class WebModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingIntKey:true,loadingReadTime:true,
            link: '',
            name: '',
            id:'',
            type:'',
            timerStart: false,
            stopwatchStart: false,
            totalDuration: 90000,
            timerReset: false,
            stopwatchReset: false,
            readTime:0
        };
        this.toggleStopwatch = this.toggleStopwatch.bind(this);
        this.resetStopwatch = this.resetStopwatch.bind(this);
    }
    componentDidMount(){
       
    }

    showWebModal = (link, name,type) => {
        var id;
        this.setState({ link: link, name: name,type:type })
        this.refs.webModal.open();
        userId = firebase.auth().currentUser.uid;
        userRef = firebase.database().ref(`/users/${userId}`);
        intAreaRef = userRef.child(`intArea/`);
        intAreaRef.on('value', (childSnapshot) => {
            childSnapshot.forEach((doc) => {
                console.log("Reading about =")
                if(type == doc.toJSON().name ){
                console.log("Reading about - ",doc.toJSON().name, " - ", doc.key) 
                id = doc.key      
                this.setState({ id: doc.key,loadingIntKey: false,})
            }
            });
        });
        var readTimeRef = firebase.database().ref('users/' + userId + '/intArea/' + id + '/readTime');
        readTimeRef.once('value').then(snapshot => {
            console.log("Last Int Read time value ", snapshot.val())
            this.setState({ readTime: snapshot.val(), loadingReadTime:false })
        })
        
    }
    headerCenter = () => {
        return (
            <View>
                <Text style={{ textAlign: 'center', color: 'black', fontSize: 10, fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null }}>{this.state.link}</Text>
            </View>
        );
    };
    headerLeft = () => {
        return (
            <View>
                <Stopwatch laps start={this.state.stopwatchStart}
                    reset={this.state.stopwatchReset}
                    options={options}
                    getMsecs={(time) => {this.getFormattedTime(time)}}
                />
            </View>
        );
    };
    toggleStopwatch() {
        this.setState({ stopwatchStart: !this.state.stopwatchStart, stopwatchReset: false });
        console.log(screen.height)
    }

    resetStopwatch() {
        this.setState({ stopwatchStart: false, stopwatchReset: true });
    }

    getFormattedTime(time) {
        this.currentTime = time;
    };
    stoppedReading =async () =>{
        if(!this.state.loadingReadTime){
            time = this.currentTime;
            var readTimeObj = {}
            readTimeObj['/intArea/' + this.state.id +'/readTime'] = this.state.readTime + time
            console.log("Read time before ", this.state.readTime, " + ", time)
            await firebase.database().ref('/users/' + userId).update(readTimeObj)
        }
    }
    render() {
        return (
            <Modal
                ref={"webModal"}
                style={{
                    width: screen.width,
                    height: screen.height,
                    top:20
                }}
                entry="left"
                position='center'
                swipeToClose={false}
                onClosed={() => {
                    this.resetStopwatch()
                }}>
                <Header
                    leftComponent={this.headerLeft}
                    centerComponent={this.headerCenter}
                    rightComponent={<Icon name='ios-close-circle' size={30} color="black"
                        onPress={() => { console.log("Loading int key ", this.state.loadingIntKey)
                            console.log("Loading iread time ", this.state.loadingReadTime) 
                            {this.state.loadingIntKey ? null:this.stoppedReading()}
                            this.refs.webModal.close() 
                        }} style={{ right: 10}} />}
                    containerStyle={{backgroundColor:'transparent'}}
                />
                <WebView
                    onLoadEnd={() => {
                        this.toggleStopwatch()
                    }}
                    startInLoadingState={true}
                    source={{ uri: this.state.link }}
                    style={{ width: screen.width, height: screen.height, }}
                />
            </Modal>
        );
    }
}
const options = {
    container: {
    },
    text: {
        textAlign: 'center', color: 'transparent', fontSize: 14, fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null
    }
};