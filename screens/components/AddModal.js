import React, { Component } from 'react';
import {
    AppRegistry, FlatList, StyleSheet, Text, View, Image, Alert,
    Platform, TouchableHighlight, Dimensions,
    TextInput, Picker
} from 'react-native';
import Modal from 'react-native-modalbox';
import Button from 'react-native-button';
import Select from 'react-native-select-plus';
var screen = Dimensions.get('window');
export default class AddModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newIntArea: '',
            loading : true
        };
    }
    showAddModal = () => {
        this.refs.myModal.open();
    }

    render() {
        return (
            <Modal
                ref={"myModal"}
                style={{
                    justifyContent: 'center',
                    borderRadius: Platform.OS === 'ios' ? 30 : 0,
                    shadowRadius: 10,
                    width: screen.width - 80,
                    height: screen.height-300
                }}
                position='center'
                backdrop={true}
                onClosed={() => {
                    // alert("Modal closed");
                }}
            >
                <Text style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    marginTop: 40
                }}>New Interest</Text>
                    <Picker selectedValue={this.state.newIntArea} onValueChange={(text) => this.setState({ newIntArea: text })}>
                    {this.props.parentFlatList.checkInterest("Art") == false ? (<Picker.Item label="Art" value="Art" />) :null}
                    {this.props.parentFlatList.checkInterest("Education") == false ? (<Picker.Item label="Education" value="Education" />) :null}
                    {this.props.parentFlatList.checkInterest("Science") == false ? (<Picker.Item label="Science" value="Science" />) :null}
                    {this.props.parentFlatList.checkInterest("Sport") == false ? (<Picker.Item label="Sport" value="Sport" />) :null}
                    {this.props.parentFlatList.checkInterest("Medicine") == false ? (<Picker.Item label="Medicine" value="Medicine" />) :null}
                    {this.props.parentFlatList.checkInterest("Technology") == false ? (<Picker.Item label="Technology" value="Technology" />) :null}
                    {this.props.parentFlatList.checkInterest("Transportation") == false ? (<Picker.Item label="Transportation" value="Transportation" />) :null}
                    {this.props.parentFlatList.checkInterest("Building") == false ? (<Picker.Item label="Building" value="Building" />) :null}
                    {this.props.parentFlatList.checkInterest("Self-improvement") == false ? (<Picker.Item label="Self-improvement" value="Self-improvement" />) :null}
                    {this.props.parentFlatList.checkInterest("Reading") == false ? (<Picker.Item label="Reading" value="Reading" />) :null}
                    {this.props.parentFlatList.checkInterest("Mechanic") == false ? (<Picker.Item label="Mechanic" value="Mechanic" />) :null}
                    {this.props.parentFlatList.checkInterest("Movie") == false ? (<Picker.Item label="Movie" value="Movie" />) :null}
                    {this.props.parentFlatList.checkInterest("Bodybuilding") == false ? (<Picker.Item label="Bodybuilding" value="Bodybuilding" />) :null}
                    {this.props.parentFlatList.checkInterest("Religion") == false ? (<Picker.Item label="Religion" value="Religion" />) :null}
                    {this.props.parentFlatList.checkInterest("Health") == false ? (<Picker.Item label="Health" value="Health" />) :null}
                    {this.props.parentFlatList.checkInterest("Travel") == false ? (<Picker.Item label="Travel" value="Travel"  />) :null}
                    </Picker>

                <Button
                    disabled={this.state.newIntArea.length <= 1  ? true : false }
                    style={{ fontSize: 18, color: 'white' }}
                    containerStyle={{
                        padding: 8,
                        marginLeft: 70,
                        marginRight: 70,
                        height: 40,
                        borderRadius: 6,
                        backgroundColor: this.state.newIntArea.length <= 1 ? 'gray' : 'purple'
                    }}
                    onPress={() => {
                        this.props.parentFlatList.onPressAdd(this.state.newIntArea);
                        this.refs.myModal.close();
                    }}
                >
                    Add
                </Button>
            </Modal>
        );
    }
}