import React, { Component } from "react";
import {
    View,TouchableOpacity,
    Text,
    StyleSheet
} from "react-native";

class FacebookButton extends Component {
    
    render() {
     
        return (
            <TouchableOpacity
                style={styles.button}
                activeOpacity={1}>
                <Text style={styles.text}>Continue with Facebook</Text>
            </TouchableOpacity>
        );
    }
}
export default FacebookButton;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'blue',
        borderRadius: 20,
        height:40,
        margin:20,
    },
     text: {
        color: 'white',
        backgroundColor: 'transparent',
    },
});