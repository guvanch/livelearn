import React, { Component } from 'react';
import {
    AppRegistry, FlatList, StyleSheet, Text,
    Platform, TouchableHighlight, Dimensions,
    TextInput, ScrollView, View, Image
} from 'react-native';
import Modal from 'react-native-modalbox';
import Button from 'react-native-button';
import Icon from 'react-native-vector-icons/Ionicons'
import { Stopwatch } from 'react-native-stopwatch-timer'
var screen = Dimensions.get('window');
import * as firebase from 'firebase';
import { Header, Card } from 'react-native-elements';
export default class ReadContr extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            article: '',
            type: '',
            image: '',
            userId: '',
            readTime:0,
            loadingIntKey:true,loadingReadTime:true,
            id:'',
            timerStart: false,
            stopwatchStart: false,
            totalDuration: 90000,
            timerReset: false,
            stopwatchReset: false,currentUserId:""
        };
        this.toggleStopwatch = this.toggleStopwatch.bind(this);
        this.resetStopwatch = this.resetStopwatch.bind(this);
    }
    showReadContrModal = (title, article, type, image, userId) => {
        this.setState({ title: title, article: article, type: type, image: image, userId: userId })
        this.refs.readContrModal.open();
        if (this.props.parentFlatList.checkInterest(type)) { this.toggleStopwatch() }
        var id;
        userId = firebase.auth().currentUser.uid;
        userRef = firebase.database().ref(`/users/${userId}`);
        intAreaRef = userRef.child(`intArea/`);
        intAreaRef.on('value', (childSnapshot) => {
            childSnapshot.forEach((doc) => {
                if(type == doc.toJSON().name ){
                console.log("Reading about - ",doc.toJSON().name, " - ", doc.key) 
                id = doc.key      
                this.setState({ id: doc.key,loadingIntKey: false,currentUserId:userId})
            }
            });
        });
        var readTimeRef = firebase.database().ref('users/' + userId + '/intArea/' + id + '/readTime');
        readTimeRef.once('value').then(snapshot => {
            console.log("Last Int Read time value ", snapshot.val())
            this.setState({ readTime: snapshot.val(), loadingReadTime:false })
        })
        

    }
    handleInterest = () => {
        console.log("Handling Interest")
        if (this.props.parentFlatList.checkInterest(this.state.type) == false) {
            this.props.parentFlatList.addInterest(this.state.type, this.state.image)
        }
    }
    headerCenter = () => {
        return (

            <View>
                <Text style={{ textAlign: 'center', color: 'black', fontSize: 18, fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null }}>{this.state.type}</Text>
                {/* {this.state.title.length>28 ? this.state.title.substr(0,28) + "..." : this.state.title} */}
            </View>
        );
    };
    headerLeft = () => {
        return (
            <View>
                {this.props.parentFlatList.checkInterest(this.state.type) ?
                <Stopwatch laps start={this.state.stopwatchStart}
                    reset={this.state.stopwatchReset}
                    options={options}
                    getMsecs={(time) => {this.getFormattedTime(time)}}
                /> : null }
            </View>
        );
    };
    headerRight = () => {
        return (
            <Button
                style={{ fontWeight: 'bold', fontSize: this.props.parentFlatList.checkInterest(this.state.type) == false ? 14 : 12, color: this.props.parentFlatList.checkInterest(this.state.type) == false ? '#12AFFE' : 'gray' }}
                containerStyle={{
                    paddingRight: 8,
                    borderRadius: 6,
                    backgroundColor: 'transparent'
                }}
                onPress={() => {
                    this.handleInterest()
                    this.setState({stopwatchStart:true})
                }}
            >{this.props.parentFlatList.checkInterest(this.state.type) == false ? 'Interest' : 'Interested'}
            </Button>
        );
    };
    toggleStopwatch() {
        this.setState({ stopwatchStart: true, stopwatchReset: false });
    }

    resetStopwatch() {
        this.setState({ stopwatchStart: false, stopwatchReset: true });
    }

    getFormattedTime(time) {
        this.currentTime = time;
    };
    stoppedReading =async () =>{
        if(!this.state.loadingReadTime && this.props.parentFlatList.checkInterest(this.state.type)){
            time = this.currentTime;
            var readTimeObj = {}
            readTimeObj['/intArea/' + this.state.id +'/readTime'] = this.state.readTime + time
            console.log("Read time before ", this.state.readTime, " + ", time)
            await firebase.database().ref('/users/' + this.state.currentUserId).update(readTimeObj)
        }
    }
    render() {
        return (
            <Modal
                swipeToClose={false}
                ref={"readContrModal"}
                style={{
                    justifyContent: 'center',
                    shadowRadius: 10,
                    width: screen.width,
                    height: screen.height,
                    top:-7
                }}
                position='center'
                backdrop={true}
                onClosed={() => {
                    // alert("Modal closed");
                }}
            >
                <Header
                    containerStyle={{backgroundColor: 'white'}}
                    style={{ flexDirection: "row" }}
                    // leftComponent={this.headerLeft}
                    rightComponent={this.headerRight}
                    centerComponent={this.headerCenter}
                    leftContainerStyle={{ flex: 1.5 }}
                    rightContainerStyle={{ flex: 1.5 }}
                    centertContainerStyle={{ flex: 2 }}
                />

                <ScrollView showsVerticalScrollIndicator={false}>
                    <Card
                        style={{fontFamily:'Arial Hebrew'}}
                        title={this.state.title}
                        image={{ uri: this.state.image }}>
                        <Text style={{ marginBottom: 10 }}>
                            {this.state.article}
                        </Text>
                        <Button
                            icon={<Icon name='ios-create' color='#ffffff' size={24} />}
                            buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
                            title='VIEW NOW' />
                    </Card>
                    <View style={{ top: 10 }}>
                        <Button
                            style={{ fontSize: 18, color: 'black', }}
                            containerStyle={{
                                padding: 8,
                                marginLeft: 70,
                                marginRight: 70,
                                height: 40,
                                borderRadius: 6,
                                backgroundColor: 'white'

                            }}
                            onPress={() => {
                                this.stoppedReading()
                                this.refs.readContrModal.close();
                                this.setState({stopwatchStart:false})
                                
                            }}
                        >
                            Done
                </Button>
                    </View>
                    <View style={{ height: 60 }}></View>
                </ScrollView>
            </Modal>
        );
    }
}
const options = {
    container: {
    },
    text: {
        textAlign: 'center', color: 'transparent', fontSize: 14, fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null
    }
};