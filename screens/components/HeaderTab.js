import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, Text, Image} from 'react-native';
import { Icon, Button, Container, Header, Content, Left}
from 'native-base'

export default class HeaderTab extends Component {
  render() {
    return (
        <Header>
            <Left>
                <Icon name="ios-menu" onPress={() =>
                this.props.navigation.navigate('DrawerOpen')}
                />
            </Left>
        </Header>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center',
  }
});