import React, { Component } from "react";
import {
    View,
    Text, ImageBackground,
    StyleSheet, Image,
} from "react-native";

class UserImage extends Component {
    render() {
        return (
            <View style={styles.profilepicWrap}>
                <Image style={styles.profilepic}
                    source={this.props.currentUserImage} />
            </View>
        );
    }
}
export default UserImage;

const styles = StyleSheet.create({
  
    profilepicWrap: {
        height: 180,
        width: 180,
        borderRadius: 100,
        borderColor: 'rgba(0,0,0,0.5)',
        borderWidth: 16,
    },
    profilepic: {
        flex: 1,
        width: null,
        alignSelf: 'stretch',
        borderRadius: 75,
        borderColor: '#fff',
        borderWidth: 4,
    },
 
});