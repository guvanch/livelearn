import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Dimensions from 'Dimensions';
import {StyleSheet, View, Text} from 'react-native';

export default class LoginSection extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>By registering to our app, you agree to our Terms and Conditions</Text>
        {/* <Text style={styles.text}>Forgot Password?</Text> */}
      </View>
    );
  }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    top: 30,
    width: DEVICE_WIDTH,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingLeft:20,
    paddingRight:20,
  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
  },
});