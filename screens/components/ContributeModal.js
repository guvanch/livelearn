import React, { Component } from 'react';
import {
    AppRegistry, FlatList, StyleSheet, Text, View, Image, Alert,
    Platform, TouchableHighlight, Dimensions, Linking,
    TextInput, Picker, ScrollView, KeyboardAvoidingView
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import Modal from 'react-native-modalbox';
import Button from 'react-native-button';
import Select from 'react-native-select-plus';
import Icon from 'react-native-vector-icons/Ionicons'
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import { Header } from 'react-native-elements';
import FlashMessage from "react-native-flash-message";
import axios from 'axios';

var screen = Dimensions.get('window');
export default class ContributeModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newType: '',
            newName: '',
            newImage: '',
            newLink: '',
            newArticle: '',
            chooseImage:'Choose Image'
        };
    }
    componentDidMount(){
        this.getImage();
    }
    showContrModal = () => {
        this.refs.contrModal.open();
    }
    addInterest() {
        this.props.parentFlatList.onPressAdd(this.state.newIntArea);
        alert('Successfully added')
        this.refs.myModal.close();
    }
    choosePhoto = async () => {
        this.getImage()
    }
    getImage = async() =>{
        await axios.get(`https://api.unsplash.com/photos/random?count=1&client_id=c30de06da8d6216f56f5140737caf517ec685cd17944ec310e6d2c6824ca81a8`)
        .then(function (response) {
            var imageResults = response.data
            var image = imageResults[0].urls.regular
            this.setState({ newImage: image ,chooseImage: 'Not related? Choose again'})
        }.bind(this))
        .catch(function (error) {
            console.log("Error with getting image -->", error);
        })
        .finally(function () {
            console.log("image request completed for Contribution ")
        });
        // await this.getPermissionAsync();
        // this.pickImage();
    }
    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            Linking.canOpenURL('app-settings:').then(supported => {
                if (!supported) {
                    this.refs.modalFlash.showMessage({
                        message: "Enable Photo Library access on Settings",
                        description: "Settings>OneSnack>Photos",
                        type: "info", duration: 5000
                    })
                } else {
                    Alert.alert(
                        'Enable Access to Photo Library',
                        'Go to Settings',
                        [{text: 'Not now', onPress: () => {
                                    this.refs.modalFlash.showMessage({
                                        message: "Enable Access to Photo Library",
                                        description: "Settings>OneSlack>Photos",
                                        type: "info", duration: 5000
                                    })
                                }, style: 'cancel'
                            },
                            {
                                text: 'Enable', onPress: () => {
                                Linking.openURL('app-settings:');
                                }
                            },
                        ],
                        { cancelable: true }
                    );
                }
            }).catch(err => console.error('An error occurred', err));

        }
    }
    pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
        });

        console.log(result);

        if (!result.cancelled) {
            this.setState({ newImage: result.uri });
            console.log(result.uri)
        }
    };
    headerCenter = () => {
        return (
            <View>
                <Text style={{ textAlign: 'center', color: 'black', fontSize: 20, fontWeight: 'bold', fontFamily:Platform.OS=='IOS' ? 'Courier New' : null}}>Contribution</Text>
            </View>
        );
    };

    render() {
        return (

            <Modal
                ref={"contrModal"}
                swipeToClose={false}
                style={{
                    shadowRadius: 10,
                    width: screen.width,
                    height: screen.height,
                    paddingTop: 20,
                    paddingBottom: 20
                }}
                position='center'
                onClosed={() => {
                    // alert("Modal closed");
                }}>
                <KeyboardAvoidingView behavior="padding" style={styles.container} keyboardVerticalOffset={100} enabled>
                    <Header
                        backgroundColor='white'
                        centerComponent={this.headerCenter}
                        rightComponent={<Icon name="ios-close" size={40} style={{right:10}} color='black' onPress={() => { this.refs.contrModal.close() }} />}
                    />
                    <View>
                        <ScrollView
                            showsVerticalScrollIndicator={false}
                        >
                            <Text style={{
                                fontSize: 14,
                                textAlign: 'left',
                                fontWeight: 'bold',
                                marginTop: 20, marginLeft: 30
                            }}>*Specify topic</Text>
                            <TextInput
                                style={{
                                    height: 40,
                                    borderColor: this.state.newName.length == 0 ? 'red' : 'green',
                                    marginLeft: 30,
                                    marginRight: 30,
                                    marginTop: 20,
                                    marginBottom: 10,
                                    borderWidth: 1,
                                    borderRadius: 6,
                                    padding: 5

                                }}
                                onChangeText={(text) => this.setState({ newName: text })}
                                placeholder="Resource Name (e.g Benefits of Sport)"
                                value={this.state.newName}
                            />
                            <Text style={{
                                fontSize: 14,
                                textAlign: 'left',
                                fontWeight: 'bold',
                                marginTop: 20, marginLeft: 30
                            }}>*Select Category</Text>
                            <Picker selectedValue={this.state.newType} onValueChange={(text) => this.setState({ newType: text })}
                                style={{

                                    borderColor: this.state.newType.length == 0 ? 'red' : 'green',
                                    marginLeft: 30,
                                    marginRight: 30,
                                    marginTop: 10,
                                    marginBottom: 10,
                                    borderWidth: 1,
                                    borderRadius: 6,
                                    padding: 5

                                }}>
                                <Picker.Item label="Art" value="Art" />
                                <Picker.Item label="Education" value="Education" />
                                <Picker.Item label="Science" value="Science" />
                                <Picker.Item label="Sport" value="Sport" />
                                <Picker.Item label="Medicine" value="Medicine" />
                                <Picker.Item label="Technology" value="Technology" />
                                <Picker.Item label="Transportation" value="Transportation" />
                                <Picker.Item label="Building" value="Building" />
                                <Picker.Item label="Self-improvement" value="Self-improvement" />
                                <Picker.Item label="Reading" value="Reading" />
                                <Picker.Item label="Mechanic" value="Mechanic" />
                                <Picker.Item label="Movie" value="Movie" />
                                <Picker.Item label="Bodybuilding" value="Bodybuilding" />
                                <Picker.Item label="Religion" value="Religion" />
                                <Picker.Item label="Health" value="Health" />
                                <Picker.Item label="Travel" value="Travel" />
                            </Picker>
                            <Text style={{
                                fontSize: 14,
                                textAlign: 'left',
                                fontWeight: 'bold',
                                marginTop: 20, marginLeft: 30
                            }}>*Write an article about - {this.state.newName.length < 1 ? '...' : this.state.newName}</Text>
                            <TextInput
                                style={{
                                    height: 60,
                                    borderColor: this.state.newArticle.length <= 3 ? 'red' : 'green',
                                    marginLeft: 30,
                                    marginRight: 30,
                                    marginTop: 10,
                                    marginBottom: 10,
                                    borderWidth: 1,
                                    borderRadius: 6,
                                    padding: 5

                                }}
                                multiline={true}
                                onChangeText={(text) => this.setState({ newArticle: text })}
                                placeholder="Article (min 100 characters)"
                                value={this.state.newArticle}
                            />
                            <Text style={{
                                fontSize: 14,
                                textAlign: 'left',
                                fontWeight: 'bold',
                                marginTop: 20, marginLeft: 30
                            }}>*Choose image related to - {this.state.newType.length < 1 ? '...' : this.state.newType}</Text>
                            <Button
                                style={{ fontSize: 18, color: 'black' }}
                                containerStyle={{
                                    padding: 10,
                                    marginLeft: 30,
                                    marginRight: 30,
                                    marginTop: 10,
                                    height: 40,
                                    borderRadius: 6,
                                    borderColor: this.state.newImage.length <= 1 ? 'red' : 'green',
                                    borderWidth: 1,
                                    marginBottom: 20,
                                    padding: 5

                                }}
                                onPress={() => { this.choosePhoto() }
                                }
                            >
                                {this.state.chooseImage}
                            </Button>
                            <View style={{bottom:10}}>
                                <Image style={{ alignSelf: 'center', width: screen.width, height: 230 }}
                                    source={{ uri: this.state.newImage }} />
                            </View>
                            <Button
                                style={{
                                    fontSize: 18, color: 'white',
                                }}
                                disabled={this.state.newName.length <= 1 || this.state.newType.length <= 1 ||
                                    this.state.newImage.length <= 1 || this.state.newArticle.length <= 3 ? true : false}

                                containerStyle={{
                                    marginBottom: 100,
                                    marginLeft: 70,
                                    marginRight: 70,
                                    height: 40,
                                    borderRadius: 6,
                                    backgroundColor: this.state.newName.length <= 1 || this.state.newType.length <= 1 ||
                                        this.state.newImage.length <= 1 || this.state.newArticle.length <= 3 ? 'gray' : 'purple',
                                    padding: 8

                                }}
                                onPress={() => {
                                    this.props.parentFlatList.addContr(this.state.newName, this.state.newType, this.state.newArticle,
                                        this.state.newImage, this.state.newLink);
                                    this.refs.contrModal.close();
                                }}
                            >
                                Done
                            </Button>
                            {/* <View style={{ height: 60 }}></View> */}
                        </ScrollView>
                    </View>

                </KeyboardAvoidingView>
                <FlashMessage ref="modalFlash" position="top" />
            </Modal>

        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },


});