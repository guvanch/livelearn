import React, { Component } from "react";
import {
    View, TouchableOpacity,
    Text, Image,
    StyleSheet,ActivityIndicator
} from "react-native";
import * as firebase from 'firebase'
import facebookLogo from '../../assets/facebookLogo.png';
import * as Facebook from 'expo-facebook';
import Icon from 'react-native-vector-icons/Ionicons'


class FacebookSignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            facebookLoading: false,
            facebookSuccess: false
        }
    }
    isUserEqual(facebookAuthResponse, firebaseUser) {
        if (firebaseUser) {
            var providerData = firebaseUser.providerData;
            for (var i = 0; i < providerData.length; i++) {
                if (providerData[i].providerId === firebase.auth.FacebookAuthProvider.PROVIDER_ID &&
                    providerData[i].uid === facebookAuthResponse.userID) {
                    // We don't need to re-auth the Firebase connection.
                    return true;
                }
            }
        }
        return false;
    }

   
    onSignIn = async (type, token) => {
        try {
            // const { type, token } = await Facebook.logInWithReadPermissionsAsync(
            //     '342162599793368', // Replace with your own app id in standalone app
            //     { permissions: ['public_profile'] }
            // );
            switch (type) {
                case 'success': {
                    // Get the user's name using Facebook's Graph API
                    const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
                    const profile = await response.json();
                    // this.checkLoginState(response);
                    const credential = firebase.auth.
                        FacebookAuthProvider.credential(token)
                    firebase.auth().signInWithCredential(credential)
                    .then(function (result) {
                        console.log('user signed in');
                        if(result.additionalUserInfo.isNewUser){
                            firebase
                            .database()
                            .ref('/users/' + result.user.uid)
                            .set({
                                email: result.user.email,
                                profile_picture: result.additionalUserInfo.profile.picture,
                                first_name: result.additionalUserInfo.profile.given_name,
                                last_name: result.additionalUserInfo.profile.family_name,
                                created_at: Date.now()   
                            })
                            .then(function (snapshot){
                                //console.log('Snapshot', snapshot);
                            });
                        } else {
                            firebase.database().
                            ref('/users/' + result.user.uid)
                            .update({
                                last_logged_in: Date.now()
                            });
                        }
                    })
                    .catch(function (error) {
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        // The email of the user's account used.
                        var email = error.email;
                        // The firebase.auth.AuthCredential type that was used.
                        var credential = error.credential;
                        // ...
                    });
                    // alert(
                        // 'Logged in!',
                        // `Hi ${profile.name}!`,

                    // );

                    break;
                }
                case 'cancel': {
                    // alert(
                    //     'Cancelled!',
                    //     'Login was cancelled!',
                    // );
                    break;
                }
                default: {
                    Alert.alert(
                        'Oops!',
                        'Login failed!',
                    );
                }
            }
        } catch (e) {
            Alert.alert(
                'Oops!',
                'Login failed!',
            );
        }
    };
    signInwithFacebookAsync = async () => {
        this.setState({facebookLoading:true})
        try {
            const result = await Facebook.logInWithReadPermissionsAsync(
                '342162599793368', // Replace with your own app id in standalone app
                { permissions: ['public_profile'] }
            );
            this.setState({facebookLoading:false})
            if (result.type === 'success') {
                this.onSignIn(result.type, result.token);
                this.setState({facebookSuccess: true})
                return result.accessToken;
            } else {
                this.setState({facebookLoading:false})
                return { cancelled: true };
            }
        } catch (e) {
            this.setState({facebookLoading:false})
            return { error: true };
        }
    };

    render() {

        return (
            <TouchableOpacity
                style={styles.button}
                activeOpacity={1}
                onPress={() => this.signInwithFacebookAsync()}
            >
                <Image source={facebookLogo} style={styles.inlineImg} />
                <View style={{display:'flex', flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                    <Text style={styles.text}>Continue with Facebook</Text>
                    {this.state.facebookLoading ? (<ActivityIndicator size="small" style={{left:20}}/>):(null)}
                    {this.state.facebookSuccess ? (<Icon name='ios-checkmark-circle-outline' size={20} color="green" style={{ left: 20}} />):(null)}
                </View>
            </TouchableOpacity>
        );
    }
}
export default FacebookSignUp;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'gray',
        borderWidth: 0.3,
        borderRadius: 20,
        height: 40,
        marginHorizontal: 20
    },
    text: {
        color: 'blue',
        backgroundColor: 'transparent',
    },
    inlineImg: {
        position: 'absolute',
        zIndex: 99,
        width: 22,
        height: 22,
        left: 35,
        top: 9,
    },
});