import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, Text, Platform} from 'react-native';

import logoImg from '../../assets/logoBird.png';

export default class Logo extends Component {
  render() {
    return (
      <View style={styles.container}>
        {/* <Image source={logoImg} style={styles.image} /> */}
        <Text style={{ textAlign: 'center', color: 'orange', fontStyle:'italic',fontSize: 30, fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null }}>One{<Text style={{ textAlign: 'center', color: 'black', fontSize: 30,fontStyle:'italic', fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null }}>Snack...</Text>}</Text>
        <Text style={{ textAlign: 'center', color: 'gray', fontStyle:'italic',fontSize: 16, fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null }}>read something new</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 130,
    height: 130,
  },
  text: {
    color: 'black',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    marginTop: 20,
    fontSize: 20,
  },
});