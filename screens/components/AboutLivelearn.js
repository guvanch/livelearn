import React, { Component } from 'react';
import {
    AppRegistry, FlatList, StyleSheet, Text,
    Platform, TouchableHighlight, Dimensions,
    TextInput, ScrollView, View
} from 'react-native';
import Modal from 'react-native-modalbox';
import Button from 'react-native-button';
import Icon from 'react-native-vector-icons/Ionicons'
var screen = Dimensions.get('window');
export default class AboutLivelearn extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    showAboutModal = () => {
        this.refs.aboutLivelearnModal.open();
    }

    render() {
        return (
            <Modal
                swipeToClose={false}
                ref={"aboutLivelearnModal"}
                style={{
                    justifyContent: 'center',
                    shadowRadius: 10,
                    width: screen.width,
                    height: screen.height - 10
                }}
                position='center'
                backdrop={true}
                onClosed={() => {
                    // alert("Modal closed");
                }}
            >
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={{
                        fontSize: 24,
                        fontWeight: 'bold',
                        textAlign: 'center',
                        marginTop: 40
                    }}>About OneSnack</Text>
                    <View style={{
                        marginLeft: 30,
                        marginRight: 20,
                    }}>
                        <View style={{
                            marginTop: 20,
                            marginBottom: 10,
                            flexDirection: 'row'
                        }}>
                            <Icon name="ios-clock" size={20} color='black' style={{ right: 3, bottom: 2 }} />
                            <Text
                                style={{
                                    fontSize: 16,
                                    fontWeight: 'bold',
                                }}
                            >What is OneSnack?</Text>
                        </View>
                        <Text style={{ fontSize: 14, }}
                        >OneSnack is a continuous learning system which gives
                            resources to users everyday according to their
                        interested fields. </Text>
                        <View style={{
                            marginTop: 20,
                            marginBottom: 10,
                            flexDirection: 'row'
                        }}>
                            <Icon name="ios-information-circle" size={20} color='black' style={{ right: 3, bottom: 2 }} />
                            <Text
                                style={{
                                    fontSize: 16,
                                    fontWeight: 'bold',
                                }}
                            >What is Interest?</Text>
                        </View>
                        <Text style={{ fontSize: 14, }}
                        >Interests are terms used in our system which refer to something 
                        that user's want to learn about it.Such as sport, technology, transportation... User's can simply add or delete Interests 
                        on Interest page
                         </Text>
                         <View style={{
                            marginTop: 20,
                            marginBottom: 10,
                            flexDirection: 'row'
                        }}>
                            <Icon name="ios-book" size={20} color='black' style={{ right: 3, bottom: 2 }} />
                            <Text
                                style={{
                                    fontSize: 16,
                                    fontWeight: 'bold',
                                }}
                            >What is Resource?</Text>
                        </View>
                        <Text style={{ fontSize: 14, }}
                        >Resource is a article that is related to one of your interested area. 
                        There are two ways that resource will appear. First, the system automatically 
                        search the resource for you. Second, the contributers will upload resource. Resources can be found 
                        on Home page
                         </Text>
                        <View style={{
                            marginTop: 20,
                            marginBottom: 10,
                            flexDirection: 'row'
                        }}>
                            <Icon name="ios-finger-print" size={20} color='#EF7D0B' style={{ right: 3, bottom: 2 }} />
                            <Text
                                style={{
                                    fontSize: 16,
                                    fontWeight: 'bold',
                                }}
                            >What is Contribution?</Text>
                        </View>
                        <Text style={{ fontSize: 14, }}
                        >Contribution is leave of trail of contributers in order to help readers. On the other hand,contribution is creating resources/articles to our system and these 
                        articles will be read by users. It is important to create valuable, good quality 
                        resources, because your resources will be reacted by users.</Text>
                         <View style={{
                            marginTop: 20,
                            marginBottom: 10,
                            flexDirection: 'row'
                        }}>
                            <Icon name="ios-create" size={20} color='black' style={{ right: 3, bottom: 2 }} />
                            <Text
                                style={{
                                    fontSize: 16,
                                    fontWeight: 'bold',
                                }}
                            >How to Contribute?</Text>
                        </View>
                        <Text style={{ fontSize: 14,marginBottom:10 }}
                        >If you would like to help by creating resources/articles,
                         you may visit Profile page, and select More icon on top-right of the page. The most important
                          tips for creating new resources are uploading high quality images 
                          and high quality articles.</Text>
                          <View style={{
                            marginTop: 20,
                            marginBottom: 10,
                            flexDirection: 'row'
                        }}>
                            <Icon name="ios-cash" size={20} color='black' style={{ right: 3, bottom: 2 }} />
                            <Text
                                style={{
                                    fontSize: 16,
                                    fontWeight: 'bold',
                                }}
                            >Is it possible to get income?</Text>
                        </View>
                        <Text style={{ fontSize: 14,marginBottom:10 }}
                        >Curently, our system is not supporting income for contributers. Earning by 
                        contributing will be in the future.</Text>
                        <View style={{
                            marginTop: 20,
                            marginBottom: 10,
                            flexDirection: 'row'
                        }}>
                            <Icon name="ios-person" size={20} color='black' style={{ right: 3, bottom: 2 }} />
                            <Text
                                style={{
                                    fontSize: 16,
                                    fontWeight: 'bold',
                                }}
                            >How Privacy is protected?</Text>
                        </View>
                        <Text style={{ fontSize: 14,marginBottom:10 }}
                        >Privacy is the important concern in our system. Users can be %100 sure 
                        that their information will be protected and not shared with 3rd party systems.</Text>
                    </View>
                    <Button
                        style={{ fontSize: 18, color: 'white'}}
                        containerStyle={{
                            padding: 8,
                            marginLeft: 70,
                            marginRight: 70,
                            height: 40,
                            borderRadius: 6,
                            backgroundColor: 'blue'

                        }}
                        onPress={() => {
                            this.refs.aboutLivelearnModal.close();
                        }}
                    >
                        OK
                </Button>
                <View style={{height:50}}></View>
                </ScrollView>


            </Modal>
        );
    }
}