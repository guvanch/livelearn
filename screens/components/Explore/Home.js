import React, { Component } from "react";
import {
    View, FlatList,
    Text, Dimensions,
    StyleSheet, Linking,
    Image, TouchableOpacity, Label
} from "react-native";
import StarRating from 'react-native-star-rating'
import Icon from 'react-native-vector-icons/Ionicons'
import * as firebase from 'firebase';
import WebModal from '../WebModal'
const { height, width } = Dimensions.get('window')
var resourceRef, userId, userRef;
class Home extends Component {

    constructor(props) {
        super(props);
        this.renderItem = this.renderItem.bind(this);
        this.state = ({
            resources: [],
            loading: false,
        })
    }
    componentDidMount() {
        userId = firebase.auth().currentUser.uid;
        userRef = firebase.database().ref(`/users/${userId}`);
        resourceRef = userRef.child(`/resource`);
        resourceRef.on('value', (childSnapshot) => {
            const resources = [];
            childSnapshot.forEach((doc) => {
                resources.push({
                    key: doc.key,
                    name: doc.toJSON().name,
                    type: doc.toJSON().type,
                    link: doc.toJSON().link,
                    image: doc.toJSON().image
                });
                this.setState({
                    resources: resources.sort((a, b) => {
                        return (a.name < b.name);
                    }),
                    loading: false,
                });
            });
        });
    }
    _onPressButton() {
        alert('added to wishlist')

    }
    openLink(link) {
        console.log(link)
        this.refs.webModel.openLink(link)
    }
    renderItem(item) {
        return (
            <View>
                <View>
                    <TouchableOpacity onPress={() => { this.openLink(item.link) }} style={{ padding: 10 }}>
                        <View style={{ width: width / 2 - 10, height: width / 2 - 10, borderWidth: 0.5, borderColor: '#dddddd' }}>
                            <View style={{ flex: 1 }}>

                                <Image
                                    style={{ flex: 1, width: null, height: null, resizeMode: 'cover' }}
                                    source={{ uri: item.image }} />

                            </View>
                            <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'space-evenly', padding: 10 }}>
                                <Text style={{ fontSize: 10, color: '#b63838' }}>{item.type}</Text>
                                <Text style={{ fontSize: 12, fontWeight: 'bold' }}>{item.name}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <WebModal ref={'webModal'} parentFlatList={this} ></WebModal>
                </View>
            </View>
        );
    }
    render() {
        return (
            <FlatList horizontal
                data={this.state.resources}
                numColumns={1}
                renderItem={(({ item, key }) => this.renderItem(item))}
            />
        );
    }
}

export default Home;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});