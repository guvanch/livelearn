import React, { Component } from "react";
import {
    View, TouchableOpacity,
    Text,Image,
    StyleSheet,ActivityIndicator
} from "react-native";
import * as firebase from 'firebase'
import * as Google from 'expo-google-app-auth'
import googleLogo from '../../assets/googleLogo.png';
import Icon from 'react-native-vector-icons/Ionicons'

class GoogleSignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            googleLoading: false,
            googleSuccess: false
        }
    }
    isUserEqual = (googleUser, firebaseUser) => {
        if (firebaseUser) {
            var providerData = firebaseUser.providerData;
            for (var i = 0; i < providerData.length; i++) {
                if (
                    providerData[i].providerId ===
                    firebase.auth.GoogleAuthProvider.PROVIDER_ID &&
                    providerData[i].uid === googleUser.getBasicProfile().getId()
                ) {
                    // We don't need to reauth the Firebase connection.
                    this.props.currentUserNewUser = true
                    return true;
                }
            }
        }
        this.props.currentUserNewUser = false
        return false;
    };
    onSignIn = googleUser => {
        console.log('Google Auth Response', googleUser);
        // We need to register an Observer on Firebase Auth to make sure auth is initialized.
        var unsubscribe = firebase.auth().onAuthStateChanged(
            function (firebaseUser) {
                unsubscribe();
                // Check if we are already signed-in Firebase with the correct user.
                if (!this.isUserEqual(googleUser, firebaseUser)) {
                    // Build Firebase credential with the Google ID token.
                    var credential = firebase.auth.GoogleAuthProvider.credential(
                        googleUser.idToken,
                        googleUser.accessToken
                    );
                    // Sign in with credential from the Google user.
                    firebase
                        .auth()
                        .signInWithCredential(credential)
                        .then(function (result) {
                            console.log('user signed in ');
                            if (result.additionalUserInfo.isNewUser) {
                                firebase
                                    .database()
                                    .ref('/users/' + result.user.uid)
                                    .set({
                                        email: result.user.email,
                                        profile_picture: result.additionalUserInfo.profile.picture,
                                        first_name: result.additionalUserInfo.profile.given_name,
                                        last_name: result.additionalUserInfo.profile.family_name,
                                        created_at: Date.now()
                                    })
                                    .then(function (snapshot) {
                                        // console.log('Snapshot', snapshot);
                                    });
                            } else {
                                firebase
                                    .database()
                                    .ref('/users/' + result.user.uid)
                                    .update({
                                        last_logged_in: Date.now()
                                    });
                            }
                        })
                        .catch(function(error) {
                            // Handle Errors here.
                            var errorCode = error.code;
                            console.log(errorCode)
                            var errorMessage = error.message;
                            console.log(errorMessage)
                            // The email of the user's account used.
                            var email = error.email;
                            console.log(email)
                            // The firebase.auth.AuthCredential type that was used.
                            var credential = error.credential;
                            console.log(credential)
                            // ...
                        });
                } else {
                    console.log('User already signed-in Firebase.');
                }
            }.bind(this)
        );
    };

    signInWithGoogleAsync = async () => {
        this.setState({googleLoading:true})
        try {
            const result = await Google.logInAsync({
                androidClientId: '982715059410-vim4bni83gr2paoips8t58ff3on0c8re.apps.googleusercontent.com',
                // behavior: 'web',
                iosClientId: '982715059410-op2dm522tktbn8u29po2p6nfige20l3l.apps.googleusercontent.com',
                scopes: ['profile', 'email'],
            });
            this.setState({googleLoading:false})
            if (result.type === 'success') {
                this.onSignIn(result);
                this.setState({googleSuccess:true})
                return result.accessToken;
            } else {
                this.setState({googleLoading:false})
                return {cancelled: true };
            }
        } catch (e) {
            this.setState({googleLoading:false})
            return { error: true };
        }
    };

    render() {

        return (
            <TouchableOpacity
                style={styles.button}
                activeOpacity={1}
                onPress={() => this.signInWithGoogleAsync()}
            >
                <Image source={googleLogo} style={styles.inlineImg} />
                <View style={{display:'flex', flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                    <Text style={styles.text}>Continue with Google</Text>
                    {this.state.googleLoading ? (<ActivityIndicator size="small" style={{left:20}}/>):(null)}
                    {this.state.googleSuccess ? (<Icon name='ios-checkmark-circle-outline' size={20} color="green" style={{ left: 20}} />):(null)}
                </View>
            </TouchableOpacity>
        );
    }
}
export default GoogleSignUp;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderColor:'gray',
        borderRadius: 20,
        height: 40,
        marginHorizontal: 20,
        borderWidth: 0.3,
        borderRadius: 20,
        height: 40,
        marginHorizontal: 20
    },
    text: {
        color: 'black',
        backgroundColor: 'transparent',
    },
    inlineImg: {
        position: 'absolute',
        zIndex: 99,
        width: 22,
        height: 22,
        left: 35,
        top: 9,
    },
});