import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet, ActivityIndicator,
} from "react-native";
import firebase from 'firebase';

class LoadingScreen extends Component {
    constructor(props){
        super(props);
    }
    componentDidMount(){
        this.checkIfLoggedIn();
    }

    checkIfLoggedIn = () => {
        firebase.auth().onAuthStateChanged(function(user)
        {
            console.log("new user: ",this.props.currentUserNewUser)
            if (this.props.currentUserNewUser) {
                this.props.navigation.navigate('Interest');
            } else if(user){
                this.props.navigation.navigate('Interest');
            }else {
                this.props.navigation.navigate('SignupScreen'); 
            }
        }.bind(this)
        );
    }
    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator size="large" />
            </View>
        );
    }
}
export default LoadingScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});