import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
} from "react-native";
import * as Expo from 'expo';
import * as firebase from 'firebase';
import * as Facebook from 'expo-facebook';
import {
    Container, Content, Header,
    Input, Label, Item,Button
} from 'native-base';
import Wallpaper from './components/Wallpaper';
import Logo from './components/Logo';
import Form from './components/Form';
import SignupSection from './components/SignupSection';
import ButtonSubmit from './components/ButtonSubmit';
import FacebookButton from './components/FacebookButton';
import GoogleSignUp from './components/GoogleSignUp';

class LoginScreen extends Component {
    isUserEqual = (googleUser, firebaseUser) => {
        if (firebaseUser) {
            var providerData = firebaseUser.providerData;
            for (var i = 0; i < providerData.length; i++) {
                if (providerData[i].providerId === firebase.auth.GoogleAuthProvider.PROVIDER_ID &&
                    providerData[i].uid === googleUser.getBasicProfile().getId()) {
                    // We don't need to reauth the Firebase connection.
                    return true;
                }
            }
        }
        return false;
    }

    onSignIn = googleUser => {
        console.log('Google Auth Response', googleUser);
        // We need to register an Observer on Firebase Auth to make sure auth is initialized.
        var unsubscribe = firebase.auth().onAuthStateChanged(function (firebaseUser) {
            unsubscribe();
            // Check if we are already signed-in Firebase with the correct user.
            if (!this.isUserEqual(googleUser, firebaseUser)) {
                // Build Firebase credential with the Google ID token.
                var credential = firebase.auth.GoogleAuthProvider.credential(
                    googleUser.idToken,
                    googleUser.accessToken
                );
                // Sign in with credential from the Google user.
                firebase.auth().signInWithCredential(credential)
                    .then(function (result) {
                        console.log('user signed in');
                        if (result.additionalUserInfo.isNewUser) {
                            firebase.database().ref('/users/' + result.user.uid)
                                .set({
                                    gmail: result.user.email,
                                    profile_picture: result.additionalUserInfo.profile.picture,
                                    locale: result.additionalUserInfo.profile.locale,
                                    first_name: result.additionalUserInfo.profile.given_name,
                                    last_name: result.additionalUserInfo.profile.family_name,
                                    created_at: Date.now()
                                })
                                .then(function (snapshot) {
                                    // console.log('Snapshot, snapshot);                        })
                                });
                        } else {
                            firebase.database().ref('/users/' + result.user.uid).update({
                                last_logged_in: Date.now()
                            })
                        }
                    })

                    .catch(function (error) {
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        // The email of the user's account used.
                        var email = error.email;
                        // The firebase.auth.AuthCredential type that was used.
                        var credential = error.credential;
                        // ...
                    });
            } else {
                console.log('User already signed-in Firebase.');
            }
        }.bind(this));
    };

    signInWithGoogleAsync = async () => {
        try {
            const result = await Expo.Google.logInAsync({
                // androidClientId: YOUR_CLIENT_ID_HERE,
                // behavior: 'web',
                iosClientId: '982715059410-op2dm522tktbn8u29po2p6nfige20l3l.apps.googleusercontent.com',
                scopes: ['profile', 'email'],
            });

            if (result.type === 'success') {
                this.onSignIn(result);
                return result.accessToken;
            } else {
                return { cancelled: true };
            }
        } catch (e) {
            return { error: true };
        }
    }
    logInWithFacebookAsync = async () => {
        try {
            const {
                type,
                token,
                expires,
                permissions,
                declinedPermissions,
            } = await Facebook.logInWithReadPermissionsAsync('342162599793368', {
                permissions: ['public_profile'],
            });
            if (type === 'success') {
                // Get the user's name using Facebook's Graph API
                const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
                Alert.alert('Logged in!', `Hi ${(await response.json()).name}!`);
            } else {
                // type === 'cancel'
            }
        } catch ({ message }) {
            alert(`Facebook Login Error: ${message}`);
        }
    }


    render() {
        return (
            <Wallpaper>
                <Logo />
                <Form />
                <SignupSection />
                <ButtonSubmit
                    buttonText='Login'
                />
                <Button
                    title="Google Sign in"
                    style={styles.button}
                    onPress={() => this.signInWithGoogleAsync()}
                />
                {/* <FacebookButton />
                <GoogleButton /> */}
            </Wallpaper>
        );
    }
}
export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
    ,button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderRadius: 20,
        height:40,
        margin:20,
    },
});
//  <Button
// title="Google Sign in"
// onPress={() => this.signInWithGoogleAsync()}
// />
// <Button
// title="Facebook Sign In"
// onPress={() => this.logInWithFacebookAsync()}
// /> 
// <Container>
//                 <Form>
//                     <Item floatingLabel>
//                         <Label>Email</Label>
//                         <Input
//                             autoCorrect={false}
//                             autoCapitalize="none"
//                         />
//                     </Item>
//                     <Item floatingLabel>
//                         <Label>Password</Label>
//                         <Input
//                             autoCorrect={false}
//                             autoCapitalize="none"
//                         />
//                     </Item>
//                 </Form>
//             </Container>



