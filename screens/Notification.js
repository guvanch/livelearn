import React, { Component } from "react";
import {
    View,
    Text, ActivityIndicator, Platform,
    StyleSheet, FlatList, Dimensions
} from "react-native";
import * as firebase from 'firebase';
const { width } = Dimensions.get('window')
import { Header } from 'react-native-elements';
import SkeletonContent from "react-native-skeleton-content";

class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            nots: [],
            loading: true,
        })
    }
    async componentDidMount() {
        currentUser = firebase.auth().currentUser
        userId = await firebase.auth().currentUser.uid;
        userRef = firebase.database().ref(`/users/${userId}`);
        notRef = userRef.child(`notifications`);
        notRef.on('value', (childSnapshot) => {
            const nots = [];
            childSnapshot.forEach((doc) => {
                nots.push({
                    key: doc.key,
                    name: doc.toJSON().name,
                    time: doc.toJSON().time,
                    date: doc.toJSON().date
                });
                this.setState({
                    nots: nots,
                    loading: false,
                });
            });
        });
    }
    headerLeft = () => {
        return (
            <View>
                <Text style={{ textAlign: 'center', color: 'black', fontSize: 24, fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null }}>Notifications</Text>
            </View>
        );
    };
    render() {
        return (
            <View style={styles.container}>
                <View style={{ paddingHorizontal: 20, marginBottom: 20 }}>

                    <Header
                        backgroundColor='white'
                        leftComponent={this.headerLeft}
                        leftContainerStyle={{ flex: 14 }}
                    />
                    {this.state.loading ? (
                        <SkeletonContent
                            containerStyle={{ flex: 1, alignItems: 'center' }}
                            isLoading={this.state.loading}
                            layout={[
                                { key: "Nots1", width: width - 40, height: 80, marginBottom: 20 , marginTop:20},
                                { key: "Nots2", width: width - 40, height: 80, marginBottom: 20 },
                                { key: "Nots3", width: width - 40, height: 80, marginBottom: 20 },
                                { key: "Nots4", width: width - 40, height: 80, marginBottom: 20 },
                                { key: "Nots5", width: width - 40, height: 80, marginBottom: 20 },
                                { key: "Nots6", width: width - 40, height: 80, marginBottom: 20 },
                            ]}
                        />) : (
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                data={this.state.nots}
                                renderItem={({ item, key }) =>
                                    <View style={{ width: width - 40, height: 80, marginTop: 20, borderWidth: 0.5, borderColor: '#dddddd', borderRadius: 5, }}>
                                        <View style={{ paddingLeft: 10, paddingTop: 10, paddingBottom: 10 }}>
                                            <Text >{item.name}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                            <View style={{ paddingLeft: 10, paddingTop: 5, paddingBottom: 5 }}>
                                                <Text >{item.time}</Text>
                                            </View>
                                            <View style={{ paddingLeft: 10, paddingTop: 5, paddingBottom: 5, right: 10 }}>
                                                <Text >{item.date}</Text>
                                            </View>
                                        </View>
                                    </View>
                                }
                            />
                        )}
                </View>
            </View>

        );
    }
}
export default Notification;

const styles = StyleSheet.create({
    container: {
        flex: 1,

    }
});