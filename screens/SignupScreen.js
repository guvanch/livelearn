import React, { Component } from "react";
import {
    View, TouchableOpacity,
    Text, KeyboardAvoidingView, Animated,
    Easing,
    StyleSheet, TextInput, Image,
} from "react-native";
import Wallpaper from './components/Wallpaper';
import Logo from './components/Logo';
import Dimensions from 'Dimensions';
import Form from './components/Form';
import LoginSection from './components/LoginSection';
import ButtonSubmit from './components/ButtonSubmit';
import emailImg from '../assets/email2.png';
import usernameImg from '../assets/username.png';
import passwordImg from '../assets/password.png';
import spinner from '../assets/loading.gif';
import { Actions, ActionConst } from 'react-native-router-flux';
import * as firebase from 'firebase';
import GoogleSignUp from './components/GoogleSignUp'
import FacebookSignUp from './components/FacebookSignUp'

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const MARGIN = 40;
var username = '';

class SignupScreen extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            showPass: true,
            press: false,
            email: '',
            password: '',
            username: '',
        };
        this.showPass = this.showPass.bind(this);
        this.state = {
            isLoading: false,
        };

        this.buttonAnimated = new Animated.Value(0);
        this.growAnimated = new Animated.Value(0);
        this._onPress = this._onPress.bind(this);
    }

    _onPress( email, password) {
        if (this.state.isLoading) return;

        this.setState({ isLoading: true });
        Animated.timing(this.buttonAnimated, {
            toValue: 1,
            duration: 200,
            easing: Easing.linear,
        }).start();

        setTimeout(() => {
            this._onGrow();
        }, 2000);
        setTimeout(() => {
            username = this.state.username
            this.signUpUser(email, password);
            this.setState({ isLoading: false });
            this.buttonAnimated.setValue(0);
            this.growAnimated.setValue(0);
        }, 2300);
    }

    _onGrow() {
        Animated.timing(this.growAnimated, {
            toValue: 1,
            duration: 200,
            easing: Easing.linear,
        }).start();
    }


    showPass() {
        this.state.press === false
            ? this.setState({ showPass: false, press: true })
            : this.setState({ showPass: true, press: false });
    }

    signUpUser = (email, password) => {
        try {

            if (this.state.password.length < 6) {
                alert('Enter at least 6 characters');
                return;
            }
            firebase.auth().createUserWithEmailAndPassword
                (email, password).then(function (result) {
                    console.log('user signed in');
                    if (result.additionalUserInfo.isNewUser) {
                        firebase.database().ref('/users/' + result.user.uid)
                            .set({
                                username: username,
                                email: result.user.email,
                                created_at: Date.now()
                            })
                            .then(function (snapshot) {
                                // console.log('Snapshot, snapshot);                        })
                            });
                    } else {
                        firebase.database().ref('/users/' + result.user.uid).update({
                            last_logged_in: Date.now()
                        })
                    }
                })
        } 
        catch (error) {
            console.log(error.toString())
        }
    }

    render() {
        const changeWidth = this.buttonAnimated.interpolate({
            inputRange: [0, 1],
            outputRange: [DEVICE_WIDTH - MARGIN, MARGIN],
        });
        const changeScale = this.growAnimated.interpolate({
            inputRange: [0, 1],
            outputRange: [1, MARGIN],
        });

        return (
            <View style={styles.container}>
                <Logo /> 
                <FacebookSignUp />
                <View style={styles.or}><Text>or</Text></View>
                <GoogleSignUp />
                <LoginSection />
            </View> 
        );
    }
}
export default SignupScreen;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    
    },
    buttonGoogle: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderRadius: 20,
        height: 40,
        margin: 20,
    },
    or:{
        alignItems: "center",
        justifyContent: 'center',
        marginTop:20,
        marginBottom: 20
    },
    circle: {
        height: MARGIN,
        width: MARGIN,
        marginTop: -MARGIN,
        borderWidth: 1,
        borderColor: '#F035E0',
        borderRadius: 100,
        alignSelf: 'center',
        zIndex: 99,
        backgroundColor: '#F035E0',
    },
    textSubmitButton: {
        color: 'white',
        backgroundColor: 'transparent',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F035E0',
        height: MARGIN,
        borderRadius: 20,
        zIndex: 100,
    },
    text: {
        color: 'black',
        backgroundColor: 'transparent',
    },
    input: {
        backgroundColor: 'rgba(255, 255, 255, 0.4)',
        width: DEVICE_WIDTH - 40, 
        height: 40,
        marginHorizontal: 20,
        paddingLeft: 45,
        borderRadius: 20,
        color: '#ffffff',
    },
    inputWrapper: {
        flex: 1,
        marginBottom: 30,
        paddingBottom:20,
    },
    inlineImg: {
        position: 'absolute',
        zIndex: 99,
        width: 22,
        height: 22,
        left: 35,
        top: 9,
    },
    image: {
        width: 24,
        height: 24,
    },
});