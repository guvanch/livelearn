import React, { Component } from "react";
import {
    View, ScrollView, FlatList,
    Text, Image, Dimensions, Platform,
    StyleSheet, Share,ActivityIndicator
} from "react-native";
import TouchableScale from 'react-native-touchable-scale';
import * as firebase from 'firebase';
import UserImage from './components/Profile/UserImage'
const { height, width } = Dimensions.get('window')
import Icon from 'react-native-vector-icons/Ionicons'
import { SearchBar, ListItem, List } from 'react-native-elements';
import WebModal from './components/WebModal';
import ActionSheet from 'react-native-actionsheet';
import ContributeModal from './components/ContributeModal';
import PureChart from 'react-native-pure-chart';
import AboutLivelearn from './components/AboutLivelearn';
import Communications from 'react-native-communications';
import ReadContr from './components/ReadContr';
import { Header } from 'react-native-elements';
import SkeletonContent from "react-native-skeleton-content";


var currentUser, userId, contrRef;
class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            contrs: [],
            loadingChart: true,
            loadingContrs: true,
            valueSearchBar: '',
            dataSearchBar: [],
            focusSearch: false,
            intAreas: [],
            totalReadTime: 0
        })
    }
    onSignOut() {
        firebase.auth().signOut()
    }

    componentDidMount() {
        currentUser = firebase.auth().currentUser
        userId = firebase.auth().currentUser.uid;
        contrRef = firebase.database().ref(`myContrs/`);
        contrRef.on('value', (childSnapshot) => {
            const contrs = [];
            childSnapshot.forEach((doc) => {
                if (userId == doc.toJSON().userId) {
                    contrs.push({
                        key: doc.key,
                        name: doc.toJSON().name,
                        image: doc.toJSON().image,
                        link: doc.toJSON().link,
                        type: doc.toJSON().type,
                        article: doc.toJSON().article,
                        userId: doc.toJSON().userId
                    });
                }
                this.setState({
                    contrs: contrs, loadingContrs: false
                });
            });
        });
        userRef = firebase.database().ref(`/users/${userId}`);
        intAreaRef = userRef.child(`/intArea`);
        intAreaRef.on('value', (childSnapshot) => {
            const intAreas = [];
            const intAreasReadTime = [];
            childSnapshot.forEach((doc) => {
                intAreas.push({
                    key: doc.key,
                    name: doc.toJSON().name,
                    image: doc.toJSON().image,
                    readTime: doc.toJSON().readTime
                });
                intAreasReadTime.push(doc.toJSON().readTime);
                this.setState({
                    intAreas: intAreas.sort((a, b) => {
                        return (a.name < b.name);
                    }), loadingChart: false
                });
                let sum = 0;
                for (let num of intAreasReadTime) {
                    sum = sum + num
                }
                this.setState({ totalReadTime: sum })
            });
        });
    }
    openContr(name, article, type, image) {
        this.refs.readContrModal.showReadContrModal(name, article, type, image);
    }
    searchFilterFunction = text => {
        this.setState({ valueSearchBar: text, focusSearch: true });
        const newData = this.state.contrs.filter(item => {
            const itemData = `${item.name} `;

            const textData = text;

            return itemData.indexOf(textData) > -1;
        });
        this.setState({ dataSearchBar: newData });

    };
    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: '86%',
                    marginLeft: '14%',
                    backgroundColor: 'transparent',
                }}
            />
        );
    };
    renderHeader = () => {
        return (
            <SearchBar
                placeholder="search your contributions..."
                containerStyle={{ backgroundColor: 'white' }}
                lightTheme
                round
                onChangeText={text => this.searchFilterFunction(text)}
                autoCorrect={false}
                value={this.state.valueSearchBar}
                clearIcon={{ color: 'white' }}
                showCancel={{ visible: true }}
            />
        );
    };
    openContrModal() {
        this.refs.contrModal.showContrModal();
    }
    addContr(name, type, article, image, link) {
        contrRef.push({
            name: name,
            image: image,
            link: link,
            type: type,
            article: article,
            userId: userId
        });
    }
    openAboutModal() {
        // this.setState({modalVisible: true});
        this.refs.aboutLivelearnModal.showAboutModal();
    }
    onSendMessage = async (email, message) => {
        alert("Sending Message to " + email)
    }
    showActionSheet = () => {
        //To show the Bottom ActionSheet
        this.ActionSheet.show();
    };
    headerLeft = () => {
        return (
            <View>
                <Text style={{ textAlign: 'center', color: 'black', fontSize: 24, fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null }}>Me</Text>
            </View>
        );
    };
    checkInterest(type) { }//created for preventing error
    shareNow = async () => {
        try {
            const result = await Share.share({
                message: 'Hey, I you have ' + this.state.totalReadTime + ' points already. Come to learn in OneSnack now www.onesnack.com',
                title: 'Come to learn in OneSnack'
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };
    render() {
        var user = firebase.auth().currentUser;
        var username, email, photoUrl, uid;

        if (user != null) {
            user.providerData.forEach(function (profile) {
                console.log("Sign-in provider: " + profile.providerId);
                console.log("  Provider-specific UID: " + profile.uid);
                console.log("  Name: " + profile.displayName);
                username = profile.displayName;
                console.log("  Email: " + profile.email);
                email = profile.email;
                console.log("  Photo URL: " + profile.photoURL);
                photoUrl = profile.photoURL
            });

        }
        var optionArray = [
            'Contribute',
            'Contact us',
            'About us',
            'Sign Out',
            'Cancel',
        ];
        let chartData = this.state.intAreas.map(item => ({
            x: item.name,
            y: item.readTime
        }));
        return (
            <View style={styles.container}>
                <View style={{ paddingHorizontal: 20, }}>
                    <Header
                        backgroundColor='white'
                        leftComponent={this.headerLeft}
                        leftContainerStyle={{ flex: 14 }}
                        rightComponent={() => <View>
                            <Icon name='ios-more' size={30} color="black"
                                onPress={this.showActionSheet} />
                            <ActionSheet
                                ref={o => (this.ActionSheet = o)}
                                //Title of the Bottom Sheet
                                title={'More...'}
                                //Options Array to show in bottom sheet
                                options={optionArray}
                                //Define cancel button index in the option array
                                //this will take the cancel option in bottom and will highlight it
                                cancelButtonIndex={4}
                                //If you want to highlight any specific option you can use below prop
                                destructiveButtonIndex={3}
                                onPress={index => {
                                    //Clicking on the option will give you the index of the option clicked
                                    if (index == 0) { this.openContrModal() }
                                    else if (index == 1) {
                                        Communications.email(['guwanchbayryyev@gmail.com'], null,
                                            null, 'Contact OneSnack', 'Hi, I need some help...')
                                        {/*email(to, cc, bcc, subject, body)*/ }
                                    }
                                    else if (index == 2) { this.openAboutModal() }
                                    else if (index == 3) { this.onSignOut() }
                                }}
                            />
                        </View>
                        }
                        rightContainerStyle={{ flex: 2 }}
                    />
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <UserImage
                        currentUserImage={{ uri: photoUrl }} />
                    <Text style={styles.name}>{username}</Text>
                    <Text>Total Points: {this.state.totalReadTime}</Text>
                    <Text>Share Now</Text><Icon name='ios-share-alt' size={30} color="blue"
                        onPress={this.shareNow} />
                </View>
                <ScrollView>
                    <View style={{ marginTop: 10 }}>
                        {this.state.loadingChart ? (
                            <SkeletonContent
                                containerStyle={{ justifyContent: 'center', alignItems: 'center' }}
                                isLoading={this.state.loadingChart}
                                layout={[
                                    { key: "Image", width: width - 10, height: 160 },
                                ]}
                            />) : (
                                <PureChart data={chartData} type='line' />
                            )}
                    </View>
                    <View style={{ top: 20, marginBottom: 20 }}>
                        <FlatList
                            style={{ marginBottom: 20 }}
                            data={this.state.focusSearch == false ? this.state.contrs : this.state.dataSearchBar}
                            renderItem={({ item }) => (
                                <ListItem
                                    roundAvatar
                                    title={item.name}
                                    subtitle={item.type}
                                    leftAvatar={{ source: { uri: item.image } }}
                                    containerStyle={{ borderBottomWidth: 0 }}
                                    chevron={{ color: 'black' }}
                                    onPress={() => this.openContr(item.name, item.article, item.type, item.image)}
                                    Component={TouchableScale}
                                    friction={90} //
                                    tension={100} // These props are passed to the parent component (here TouchableScale)
                                    activeScale={0.95} //
                                />
                            )}
                            // keyExtractor={item => item.name}
                            ItemSeparatorComponent={this.renderSeparator}
                            ListHeaderComponent={this.renderHeader}
                        />
                    </View></ScrollView>
                <ReadContr ref={'readContrModal'} parentFlatList={this} ></ReadContr>
                <ContributeModal ref={'contrModal'} parentFlatList={this} ></ContributeModal>
                <AboutLivelearn ref={'aboutLivelearnModal'} parentFlatList={this} ></AboutLivelearn>
            </View>

        );
    }
}
export default Profile;
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerBackground: {
        flex: 1,
        width: null,
        alignSelf: 'stretch',
    },
    profilepicWrap: {
        height: 180,
        width: 180,
        borderRadius: 100,
        borderColor: 'purple',
        borderWidth: 16,
    },
    profilepic: {
        flex: 1,
        width: null,
        alignSelf: 'stretch',
        borderRadius: 75,
        borderColor: 'purple',
        borderWidth: 4,
    },
    name: {
        marginTop: 20,
        fontSize: 16,
        color: 'black',
        fontWeight: 'bold',
    },
    position: {
        fontSize: 14,
        color: '#0394c0',
        fontWeight: '300',
        fontStyle: 'italic',

    }

});