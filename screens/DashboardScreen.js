import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet, Button,Image
} from "react-native";
import * as firebase from 'firebase';


class DashboardScreen extends Component {

    render() {
        var user = firebase.auth().currentUser;
        var username, email, photoUrl, uid;

        if (user != null) {
          user.providerData.forEach(function (profile) {
            console.log("Sign-in provider: " + profile.providerId);        
            console.log("  Provider-specific UID: " + profile.uid);
            console.log("  Name: " + profile.displayName);
            username = profile.displayName;
            console.log("  Email: " + profile.email);
            email = profile.email;
            console.log("  Photo URL: " + profile.photoURL);
            photoUrl = profile.photoURL
          });
        }
        return (
            <View style={styles.container}>
                <Text> Hey! {username}</Text>
                <Text> Your email is {email}! </Text>
                <Text> Your profile pic is: </Text>
                <Image source={{uri: photoUrl}} style={styles.image} />
                <Button title="Sign out" onPress={() =>
                firebase.auth().signOut()} />
            </View>
        );
    }
}
export default DashboardScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: 130,
        height: 130,
      },
});