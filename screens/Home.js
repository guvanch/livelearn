import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    Platform,
    ScrollView,
    Image, TextInput,
    Dimensions, FlatList, WebView, ActivityIndicator,
    Animated, TouchableOpacity, Linking
} from "react-native";
import Button from 'react-native-button';
import TouchableScale from 'react-native-touchable-scale';
import { SearchBar, ListItem, List, Header } from 'react-native-elements';
import WebModal from './components/WebModal';
import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons'
import * as firebase from 'firebase';
const { height, width } = Dimensions.get('window')
var userRef, currentUser, userDayCount, intAreaRef;
import ReadContr from './components/ReadContr';
import { Keyboard } from 'react-native'
import FlashMessage from "react-native-flash-message";
import SkeletonContent from "react-native-skeleton-content";

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.getNow = this.getNow.bind(this);
        this.sendPushNotification = this.sendPushNotification.bind(this);
        this.state = ({
            intAreas: [],
            resources: [],
            resourceLink: '',
            resourceImage: '',
            resourceType: '',
            resourceName: '',
            imageResults: '',
            dayCount: 1,
            currentHour: '',
            currentMin: '',
            searchTerms: '',
            recentResource: '',
            loading: true,
            loadingInterests: true,
            loadingResources: true,
            dataSearchBar: [],
            valueSearchBar: '',
            focusSearch: false,
            contrs: [],
            lastVisitDate: 0, gettingResource: false, noIntArea: false, noResource: false
        })
    }
    componentDidMount = async () => {
        currentUser = firebase.auth().currentUser
        userId = firebase.auth().currentUser.uid;
        userRef = firebase.database().ref(`/users/${userId}`);
        intAreaRef = userRef.child(`intArea`);
        intAreaRef.on('value', (childSnapshot) => {
            const intAreas = [];
            childSnapshot.forEach((doc) => {
                intAreas.push({
                    key: doc.key,
                    name: doc.toJSON().name,
                    image: doc.toJSON().image
                });
                this.setState({
                    intAreas: intAreas.sort((a, b) => {
                        return (a.name < b.name);
                    }), loadingInterests: false
                });
            });
            if (intAreas.length == 0) {
                console.log("User has no Interets")
                this.setState({ noIntArea: true, loadingInterests: false })
            }
        });
        this.refreshResources();
        contrRef = firebase.database().ref(`myContrs/`);
        contrRef.on('value', (childSnapshot) => {
            const contrs = [];
            childSnapshot.forEach((doc) => {
                if (userId != doc.toJSON().userId) {
                    contrs.push({
                        key: doc.key,
                        name: doc.toJSON().name,
                        image: doc.toJSON().image,
                        link: doc.toJSON().link,
                        type: doc.toJSON().type,
                        article: doc.toJSON().article,
                        userId: doc.toJSON().userId
                    });
                }
                this.setState({
                    contrs: contrs,
                });
            });
        });
        // get the searchTERM and save 
        var searchTermRef = firebase.database().ref('searchTerms/');
        searchTermRef.once('value').then(snapshot => {
            this.setState({ searchTerms: snapshot.val() })
        })
        // get the dayCount and save 
        var dayCountRef = firebase.database().ref('users/' + userId + '/dayCount');
        dayCountRef.once('value').then(snapshot => {
            this.setState({ dayCount: snapshot.val() })
        })
        console.log("Today's day count: ", this.state.dayCount)
        // get user's last visit time
        var lastVisitRef = firebase.database().ref('users/' + userId + '/lastVisitDate');
        lastVisitRef.once('value').then(snapshot => {
            this.setState({ lastVisitDate: snapshot.val() })
            console.log(snapshot.val())
        })
        const delay = ms => new Promise(res => setTimeout(res, ms));
        await delay(2000);
        this.checkIfUserNeedResource()
    }
    checkIfUserNeedResource = async () => {
        var date = new Date().getDate();
        console.log("Today is " + date)
        console.log("Last visit Date is : ", this.state.lastVisitDate)
        if (date === this.state.lastVisitDate) {
            console.log("Equal")
            this.refs.modalFlash.showMessage({
                message: "Today's resources are ready to read",
                description: "Keep reading",
                type: "info", duration: 5000,icon:"info",
                style: { top: 20 }
            })
        } else {
            console.log("Not equal")
            this.setState({ gettingResource: true })
            await this.getNow()
            var lastVisitObj = {}
            lastVisitObj['/lastVisitDate'] = date
            await firebase.database().ref('/users/' + userId).update(lastVisitObj)
            this.setState({ gettingResource: false })
        }
    }
    refreshResources = () => {
        resourceRef = userRef.child(`resource`);
        resourceRef.on('value', (childSnapshot) => {
            const resources = [];
            childSnapshot.forEach((doc) => {
                resources.push({
                    key: doc.key,
                    name: doc.toJSON().name,
                    image: doc.toJSON().image,
                    link: doc.toJSON().link,
                    type: doc.toJSON().type,
                });
                this.setState({
                    resources: resources.sort((a, b) => {
                        return (a.name < b.name);
                    }),
                    loadingResources: false,
                    resourceImage: doc.toJSON().image, refreshing: false,
                    resourceLink: doc.toJSON().link, resourceName: doc.toJSON().name
                });
            });
            if (resources.length == 0) {
                console.log("User has no Resources")
                this.setState({ noResource: true, loadingResources: false })
            }
        });
    }
    openLink(link, name, type) {
        this.refs.webModal.showWebModal(link, name, type);
    }
    _onPressButton() {
        this.TextInput = ''
    }
    getNow = async () => {

        this.refs.modalFlash.showMessage({
            message: "Preparing new resources...",
            description: "Do not close OneSnack",
            type: "info", duration: 5000,icon:"info",
            style: { top: 20 }
        })
        // get user today's search term
        dayCount = this.state.dayCount
        var searchTerm = this.state.searchTerms[dayCount]
        console.log("Today's search term: ", this.state.searchTerms[dayCount])

        const delay = ms => new Promise(res => setTimeout(res, ms));
        var intAreas = this.state.intAreas
        for (i = 0; i < intAreas.length; i++) {
            console.log('Getting resource for - ', searchTerm, " ", intAreas[i].name)
            await this.getResource(intAreas[i].name, searchTerm);
            this.sendPushNotification();
            await delay(5000);
        }
        // after getting resource, update daycount for next search term
        var dayCountObj = {}
        dayCountObj['/dayCount'] = dayCount + 1
        await firebase.database().ref('/users/' + userId).update(dayCountObj)
        console.log('daycount increased by 1')
        // save notification to database
        this.refs.modalFlash.showMessage({
            message: "Successfully got new resources",
            description: "Do not forget to read all ",
            type: "success", duration: 5000,icon:"success",
            style: { top: 20 }
        })

    }

    getResource = async (intName, searchTerm) => {
        await axios.get(`https://api.unsplash.com/photos/random?count=1&client_id=c30de06da8d6216f56f5140737caf517ec685cd17944ec310e6d2c6824ca81a8`)
            .then(function (response) {
                var imageResults = response.data
                var image = imageResults[0].urls.regular
                this.setState({ resourceImage: image })
            }.bind(this))
            .catch(function (error) {
                console.log("Error with getting image -->", error);
            })
            .finally(function () {
                console.log("image request completed for ", intName)
            });


        // get google search output TITLe, LINK

        await axios.get(`https://www.googleapis.com/customsearch/v1?key=AIzaSyBtJ3rajDSL9v3ni3BYe0uOJ2l_-ZsunJI&cx=009673595878660243882:ispdej2rwqk&num=1&q=${searchTerm}+${intName}`)
            .then(function (response) {
                var googleResults = response.data;
                //get resoure name intFirst
                var name = googleResults.items[0].title
                if (name.length > 43) {
                    name = name.substring(0, 40) + '...';
                }
                //get resource link
                var link = googleResults.items[0].link
                this.setState({
                    resourceName: name, resourceLink: link,
                    resourceType: intName
                })
                resourceRef.push({
                    name: this.state.resourceName,
                    image: this.state.resourceImage,
                    link: this.state.resourceLink,
                    type: this.state.resourceType,
                });
            }.bind(this))
            .catch(function (error) {
                console.log("Error with google request--> ", error);
            })
            .finally(function () {
                console.log("google request completed for ", intName)
            });
    }
    sendPushNotification = async () => {
        let response = await fetch('https://exp.host/--/api/v2/push/send', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                to: 'ExponentPushToken[6AFaMTCr_iRslNAkfaoEDv]',
                sound: 'default',
                title: this.state.resourceName,
                body: this.state.resourceType,
            }),
        });
        console.log('Finished sending...')
    }
    convertMonth(month) {
        if (month == 1) { month = 'Jan' } if (month == 2) { month = 'Feb' } if (month == 3) { month = 'Mar' }
        if (month == 4) { month = 'Apr' } if (month == 5) { month = 'May' } if (month == 6) { month = 'Jun' }
        if (month == 7) { month = 'Jul' } if (month == 8) { month = 'Aug' } if (month == 9) { month = 'Sep' }
        if (month == 10) { month = 'Oct' } if (month == 11) { month = 'Nov' } if (month == 12) { month = 'Dec' }
        return month;
    }
    searchFilterFunction = text => {
        this.setState({ valueSearchBar: text, focusSearch: true });
        const newData = this.state.resources.filter(item => {
            const itemData = `${item.name} `;

            const textData = text;

            return itemData.indexOf(textData) > -1;
        });
        this.setState({ dataSearchBar: newData });

    };
    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: '86%',
                    marginLeft: '14%',
                    backgroundColor: 'transparent',
                }}
            />
        );
    };
    renderHeader = () => {
        return (
            <SearchBar
                placeholder="search resource..."
                containerStyle={{ backgroundColor: 'white' }}
                lightTheme
                round
                onChangeText={text => this.searchFilterFunction(text)}
                autoCorrect={false}
                value={this.state.valueSearchBar}
                showCancel={{ visible: true }}
                clearIcon={{ color: 'white' }}
            />
        );
    };

    closeSearchBar = () => {
        this.setState({ dataSearchBar: [], focusSearch: false });
        Keyboard.dismiss()
    }
    openContr(name, article, type, image, userId) {
        this.refs.readContrModal.showReadContrModal(name, article, type, image, userId);
    }
    checkInterest(intArea) {
        for (i = 0; i < this.state.intAreas.length; i++) {
            if (this.state.intAreas[i].name == intArea) {
                return true;
            }
        }
        return false;
    }
    headerCenter = () => {
        return (
            <View>
                <Text style={{ textAlign: 'center', color: 'orange', fontStyle:'italic',fontSize: 24, fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null }}>One{<Text style={{ textAlign: 'center', color: 'black', fontSize: 24,fontStyle:'italic', fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null }}>Snack</Text>}</Text>
            </View>
        );
    };

    render() {
        return (
            <SafeAreaView style={{ flex: 1, top: -20 }}>
                <Header
                    backgroundColor='white'
                    // leftComponent={{ icon: 'menu', color: 'black' }}
                    centerComponent={this.headerCenter}
                    rightComponent={this.state.focusSearch == false ?
                        (
                            this.state.gettingResource ? <ActivityIndicator size="small" color="grey" /> : null// <Icon name='ios-download' size={30} color="black" onPress={() => { this.getNow() }} style={{ right: 10 }} />
                        )
                        : <Icon name='ios-close-circle-outline' size={30} color="black"
                            onPress={this.closeSearchBar} style={{ right: 10 }} />}
                />
                <View>

                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        scrollEventThrottle={16}
                        onScroll={Animated.event(
                            [
                                { nativeEvent: { contentOffset: { y: this.scrollY } } }
                            ]
                        )}
                    >
                        <View>
                            <FlatList
                                data={this.state.dataSearchBar}
                                renderItem={({ item }) => (
                                    <ListItem
                                        roundAvatar
                                        title={item.name}
                                        subtitle={item.type}
                                        subtitleStyle={{ color: 'gray' }}
                                        leftAvatar={{ source: { uri: item.image } }}
                                        containerStyle={{ borderBottomWidth: 0 }}
                                        chevron={{ color: 'black' }}
                                        onPress={() => this.openLink(item.link, item.name)}
                                        Component={TouchableScale}
                                        friction={90} //
                                        tension={100} // These props are passed to the parent component (here TouchableScale)
                                        activeScale={0.95} //

                                    />
                                )}
                                // keyExtractor={item => item.name}
                                ItemSeparatorComponent={this.renderSeparator}
                                ListHeaderComponent={this.renderHeader}

                            />
                        </View>
                        <View style={{ flex: 1, backgroundColor: 'white', paddingTop: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 10, top: 10, bottom: 10 }} >
                                <Text style={{ fontSize: 20, fontWeight: '700' }}>
                                    Interests
                                    </Text>
                            </View>

                            <View style={{ height: 130, marginTop: 20 }}>
                                {this.state.loadingInterests ? (
                                    <SkeletonContent
                                        containerStyle={{ alignItems: 'center', display: 'flex', flexDirection: 'row', justifyContent: 'space-between', overflow: 'hidden' }}
                                        isLoading={this.state.loadingInterests}
                                        layout={[
                                            { key: "interest1", width: 130, height: 130, marginRight: 20, marginLeft: 20 },
                                            { key: "Interest2", width: 130, height: 130, marginRight: 20 },
                                            { key: "Interest3", width: 130, height: 130, marginRight: 20 },
                                        ]}
                                    />) : (
                                        <FlatList horizontal
                                            style={{ marginRight: 10 }}
                                            showsHorizontalScrollIndicator={false}
                                            data={this.state.intAreas}
                                            renderItem={({ item }) =>
                                                <View style={{ height: 130, width: 130, marginLeft: 10, borderWidth: 0.5, borderRadius: 5, borderColor: 'gray' }}>
                                                    <Image
                                                        style={{ flex: 1, height: null, width: null, resizeMode: 'cover', borderRadius: 5, borderWidth: 1, borderColor: '#dddddd' }}
                                                        source={{ uri: item.image }}
                                                    />
                                                    <View style={{ paddingLeft: 10, paddingTop: 10, paddingBottom: 10 }}>
                                                        <Text >{item.name}</Text>
                                                    </View>
                                                </View>
                                            }
                                        />)
                                    }
                            </View>

                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={{ fontSize: 20, fontWeight: '700', paddingHorizontal: 10 }}>
                                Snacks
                            </Text>
                            <View style={{ paddingHorizontal: 10, marginTop: 20, flexWrap: 'wrap', justifyContent: 'space-between' }}>
                                {/* <Home width={width}
                                    /> */}
                                <View>
                                    {this.state.loadingResources ? (
                                        <SkeletonContent
                                            containerStyle={{ flex: 1, alignItems: 'center', }}
                                            isLoading={this.state.loadingResources}
                                            layout={[
                                                { key: "Resources", width: width - 40, height: 180, marginBottom: 20 },
                                            ]}
                                        />) : (
                                            <FlatList
                                                style={{ marginBottom: 40 }}
                                                data={this.state.resources}
                                                numColumns={1}
                                                renderItem={({ item, key }) =>
                                                    <View style={{borderRadius:15}}>
                                                        <TouchableOpacity onPress={() => { this.openLink(item.link, item.name, item.type) }}
                                                            Component={TouchableScale}
                                                            friction={90} //
                                                            tension={100} // These props are passed to the parent component (here TouchableScale)
                                                            activeScale={0.95} //
                                                            style={{ paddingBottom: 10 }}>
                                                            <View style={{ height: 250, borderWidth: 0.5, borderColor: '#dddddd', borderRadius:15}}>
                                                                <View style={{ flex: 1 }}>

                                                                    <Image
                                                                        style={{ flex: 1, width: null, height: null, resizeMode: 'cover',borderRadius:15}}
                                                                        source={{ uri: item.image }} />

                                                                </View>
                                                                <View style={{ alignItems: 'flex-start', justifyContent: 'space-evenly', padding: 10 }}>
                                                                    <Text style={{ fontSize: 12, color: 'gray' }}>{item.type}</Text>
                                                                    <Text style={{ fontSize: 15,fontWeight: 'bold' }}>{item.name}</Text>
                                                                </View>
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                }

                                            />)
                                        }
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
                <WebModal ref={'webModal'} parentFlatList={this} ></WebModal>
                <ReadContr ref={'readContrModal'} parentFlatList={this} ></ReadContr>
                <FlashMessage ref="modalFlash" position="bottom" />
            </SafeAreaView>

        );
    }
}
export default Home;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});