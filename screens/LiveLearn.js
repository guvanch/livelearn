import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet
} from "react-native";

class LiveLearn extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>LiveLearn</Text>
            </View>
        );
    }
}
export default LiveLearn;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});