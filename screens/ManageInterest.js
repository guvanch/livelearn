import React, { Component } from "react";
import {
    View,
    Text, Dimensions, FlatList, Platform, Alert, Modal, ImageBackground,
    StyleSheet,
} from "react-native";
import axios from 'axios';
import Swipeout from 'react-native-swipeout';
const { height, width, screen } = Dimensions.get('window')
import Category from './components/Explore/Category'
import Icon from 'react-native-vector-icons/Ionicons'
import { TouchableOpacity } from "react-native-gesture-handler";
import * as firebase from 'firebase';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions'
import { Header } from 'react-native-elements';
import FlashMessage from "react-native-flash-message";
import moment from 'moment';
import SkeletonContent from "react-native-skeleton-content";
import Button from 'react-native-button';

var userRef, userId, currentUser, intAreaRef;
class ManageInterest extends Component {
    constructor(props) {
        super(props);
        this.goToHome = this.goToHome.bind(this);
        this.state = ({
            intAreas: [],
            interests:[],
            newIntArea: '',
            newImage: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0NDw0NDQ4NDg0NDQ4NDQ0NDQ8NDQ0NFREWFhURExMYHSggGBolGxUTIjEhJSkrLi4uFx8zODM4NygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIALcBEwMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAgEDBQQH/8QAMRABAQACAAMGBAUDBQAAAAAAAAECEQMEIRIxQVFSkWFxgbETFBUiMwUjMkJiocHR/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP0QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgQG6NABpmmtBnZOyoBOjSgE6OyoBPZZpbATpugBlCgAAAAAAAAAAAAAAAAAABBsAaNBjdNbIDG6bI3QJ0aWzQJ0zTppmgQzS9MsBApgJrGsAAAAAAAAAAAAAAAAAAAbGNgKjZGLkAkbI2RUgMmKtKkVMQRo06SNmIOWk9l9WXL5ydqzU+Pf7OVgONxTY62JsBzsTYuxlgOdSqpAAAAAAAAAAAAAAAAAAAVilWILioyKgKkXIYY29JLb5SbfZweQzv+WsZ70HzSO3C4GWXdL8/B9/C5Xh4+Havneqs7xP9Mxnxt2DhhyUnXPL6TpPdX4vDw/wm78P/U5cDiXvsv1T+Vz+HuDtws/xccsbNeH0edljrp5XVehwODljlvpruvVPMcrcsrcdavnddQedYix9nF5TOS261Pi+WwHLKIsdKig5ZIXkigAAAAAAAAAAAAAAAAAAKxSrEHSLxRF4g+rkeJ2c55X9t+r6+eyzlk3ezZ3Tp1ebHq8T+5wpl4zr9Z3g3lrrhWzvnarljxuJem7fhqOnL/xZfLL7I5TiTG3fjO8G3i8Sd9s+cjPxs/P7L5riY3UnXXi4yb6TxBd42fq+ybx8/Vf+Hficv+2a62d/xfLxMbjdXvB9XauXCtt3dXr9XmZPRx/hvyy+7zqCMnLJ0yc8gc83OumTmAAAAAAAAAAAAAAAAAAArFKsQdIqIlVAdZXof0zid+F8es/7ebK7cDidnKZeVB6mOHZ4fEnl2tfLT4ZXpce/syv+2/Z5vB4mMu8pvXhsH04cvlZvuvhL5O3LcHXWzr4Tyc/z89N9z89PTfcH2Pi5/HrMvpW/n56b7uXMc3M8ddmy9LLsHXG/2L8svu8216GH8F+WX3ebaDK51WVRQc8kLqAAAAAAAAAAAAAAAAAAAGxjYClyobKDpKqVzlaD0uBz/ZxmNxts6b34Os/qU9N93lSqlB6n6jPTfc/UJ6L7x5mzYPT/AFGem+7P1Kem+7zdstB9/H5+ZY5Y9mzc1vb4LU2sAqbS1OwTUqqQAAAAAAAAAAAAAAAAAACBAU1LQU2VIC5Vbc9mwdNm0do2C7WbT2mbBdqbWbYBsYAxjawAAAAAAAAAAAAAAAAAAAgQGgA0Y0GjGgNYAAwGsAGAAVjawAAAAAAAAAAAAAAAAAAAAGwZG7ADZsGjNmwaM2bBozZsGsNmwA2bArCgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/2Q==',
            loading: true,
            activeRowKey: null,
            currentIntArea: null,
            currentIntAreaName: null,
            noIntArea: false,
        })
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    goToHome() {
        this.props.navigation.navigate('Home');
    }
    async componentDidMount() {
        currentUser = firebase.auth().currentUser
        await this.registerForPushNotificationsAsync(currentUser)
        userId = await firebase.auth().currentUser.uid;
        userRef = firebase.database().ref(`/users/${userId}`);
        intAreaRef = userRef.child(`/intArea`);
        if (intAreaRef == null) {
            alert('nul')
        }
        intAreaRef.on('value', (childSnapshot) => {
            const intAreas = [];
            childSnapshot.forEach((doc) => {
                intAreas.push({
                    key: doc.key,
                    name: doc.toJSON().name,
                    newImage: doc.toJSON().newImage
                });
                this.setState({
                    intAreas: intAreas.sort((a, b) => {
                        return (a.name < b.name);
                    }),
                    loading: false,
                });
            });
            if (intAreas.length == 0) {
                this.setState({ loading: false, noIntArea: true })
            }
        });
        this.scheduleLocalNotifications()
        // get the interests and save 
        var interestRef = firebase.database().ref('interests/');
        interestRef.once('value').then(snapshot => {
            this.setState({ interests: snapshot.val() })
        })
    }
    scheduleLocalNotifications = async () => {
        const localNotification = {
            title: 'Delayed testing Title',
            body: 'Testing body',
            data: { type: 'delayed' }
        }
        const schedulingOptions = {
            time: (new Date()).getTime() + 5000
        }
        // console.log('Scheduling delayed notification:', { localNotification, schedulingOptions })
        // Notifications.scheduleLocalNotificationAsync(localNotification, schedulingOptions)
        //     .then(id => 
        //         console.info(`Delayed notification scheduled (${id}) at ${moment(schedulingOptions.time).format()}`))
        //     .catch(err => console.error(err))
    }
    registerForPushNotificationsAsync = async (currentUser) => {
        const { existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
        let finalStatus = existingStatus;

        // only ask if permissions have not already been determined, because
        // iOS won't necessarily prompt the user a second time.
        if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }

        // Stop here if the user did not grant permissions
        if (finalStatus !== 'granted') {
            return;
        }

        // Get the token that uniquely identifies this device
        let token = await Notifications.getExpoPushTokenAsync();

        // POST the token to our backend so we can use it to send pushes from there
        var updates = {}
        updates['/expoToken'] = token
        await firebase.database().ref('/users/' + currentUser.uid).update(updates)
        //call the push notification 
    }

    sendPushNotification = async () => {
        let response = await fetch('https://exp.host/--/api/v2/push/send', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                to: 'ExponentPushToken[6AFaMTCr_iRslNAkfaoEDv]',
                sound: 'default',
                title: 'Demo',
                body: 'Demo notificaiton',
            }),
        });
        console.log('Finished sending...')
    }


    getImage = async (interest) => {
        this.loading = true;
        var newImage;
        if (interest == "Football") { newImage = 'https://images.unsplash.com/photo-1513364776144-60967b0f800f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80' }
        if (interest == "Education") { newImage = 'https://images.unsplash.com/photo-1497633762265-9d179a990aa6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (interest == "Science") { newImage = 'https://images.unsplash.com/photo-1507413245164-6160d8298b31?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80' }
        if (interest == "Sport") { newImage = 'https://images.unsplash.com/photo-1483721310020-03333e577078?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (interest == "Big Data") { newImage = 'https://images.unsplash.com/photo-1527613426441-4da17471b66d?ixlib=rb-1.2.1&auto=format&fit=crop&w=1035&q=80' }
        if (interest == "Human Being") { newImage = 'https://images.unsplash.com/photo-1556742521-9713bf272865?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (interest == "Living Abroad") { newImage = 'https://images.unsplash.com/photo-1519003722824-194d4455a60c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (interest == "Building") { newImage = 'https://images.unsplash.com/photo-1486406146926-c627a92ad1ab?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (interest == "Self-improvement") { newImage = 'https://images.unsplash.com/photo-1508908324153-d1a219719814?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (interest == "Reading") { newImage = 'https://images.unsplash.com/photo-1519682577862-22b62b24e493?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (interest == "Mechanic") { newImage = 'https://images.unsplash.com/photo-1517524206127-48bbd363f3d7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (interest == "Movie") { newImage = 'https://images.unsplash.com/photo-1478720568477-152d9b164e26?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (interest == "Technology") { newImage = 'https://images.unsplash.com/photo-1554344728-77cf90d9ed26?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (interest == "Religion") { newImage = 'https://images.unsplash.com/photo-1517244649640-fcacab5ce213?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (interest == "Evalution") { newImage = 'https://images.unsplash.com/photo-1476480862126-209bfaa8edc8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (interest == "Travel") { newImage = 'https://images.unsplash.com/photo-1530521954074-e64f6810b32d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        return newImage
    }
    checkInterest = (interest) => {
        let intAreas = this.state.intAreas
        for (let i = 0; i < intAreas.length; i++) {
            if (intAreas[i].name == interest) {
                return true;
            }
        }
        return false;
    }
    addInterest = async (interest) => {
        console.log("adding interest - ",interest )
        intAreaRef.push({
            name: interest,
            image: await this.getImage(interest),
            readTime: 0
        })
    }

    removeInterest = (interest) =>{
        console.log("removing interest - ",interest )
        let key = null
        intAreaRef.on('value', (childSnapshot) => {
            childSnapshot.forEach((doc) => {
                if (interest == doc.toJSON().name) {
                    firebase.database().ref(`/users/${userId}/intArea`).child(doc.key).remove();
                }
            });
        });
        resourceRef = userRef.child(`resource/`);
        resourceRef.on('value', (childSnapshot) => {
            childSnapshot.forEach((doc) => {
                if (interest == doc.toJSON().type) {
                    console.log("deleting resource about - ", interest)
                    firebase.database().ref(`/users/${userId}/resource`).child(doc.key).remove();
                }
            });
        });
    }
    handleInterest = (interest) => {
        console.log("handling interest - ", interest)
        if (this.checkInterest(interest)) {
            this.removeInterest(interest)
        } else {
            this.addInterest(interest)
        }
    }
    headerLeft = () => {
        return (
            <View>
                <Text style={{ textAlign: 'center', color: 'black', fontSize: 24, fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null }}>Choose Interests</Text>
            </View>
        );
    };

    render() {

        return (
            <View style={styles.container}>
                <View style={{ paddingHorizontal: 20 }}>
                    <Header
                        backgroundColor='white'
                        leftComponent={this.headerLeft}
                        leftContainerStyle={{ flex: 14 }}
                        rightComponent={<Icon name="ios-close-circle" size={30} color="gray"
                            onPress={() => { this.goToHome() }} style={{ right: 5, display: this.state.intAreas.length < 5 ? ('none'):null }} />}
                        rightContainerStyle={{ flex: 2 }}
                    />
                    <Text style={{ fontWeight: '100', marginTop: 8, marginBottom: 3, left: 7 }}>
                        Select at least 5 interests
                </Text>
                    {this.state.loading ? (
                        <SkeletonContent
                            containerStyle={{ flex: 1, alignItems: 'center' }}
                            isLoading={this.state.loading}
                            layout={[
                                { key: "Ints1", width: width - 40, height: 40, marginBottom: 20 },
                                { key: "Ints2", width: width - 40, height: 40, marginBottom: 20 },
                                { key: "Ints3", width: width - 40, height: 40, marginBottom: 20 },
                                { key: "Ints4", width: width - 40, height: 40, marginBottom: 20 },
                                { key: "Ints5", width: width - 40, height: 40, marginBottom: 20 },
                                { key: "Ints6", width: width - 40, height: 40, marginBottom: 20 },
                                { key: "Ints7", width: width - 40, height: 40, marginBottom: 20 },
                                { key: "Ints8", width: width - 40, height: 40, marginBottom: 20 },
                                { key: "Ints9", width: width - 40, height: 40, marginBottom: 20 },
                            ]}
                        />) :(<FlatList
                                style={{ marginBottom: 75 }}
                                showsVerticalScrollIndicator={false}
                                data={this.state.interests}
                                renderItem={({ item, key }) =>
                                        <View style={{ padding:5, marginTop: 15, display:'flex',flexDirection:'row', justifyContent:'space-between',alignItems:'center',paddingVertical:5, borderBottomWidth:0.3, borderBottomColor:'gray'}}>
                                            <Text style={{fontSize: 14,color: 'black',}} >{item}</Text>
                                            <Button
                                                style={{ fontWeight: 'bold', fontSize: 14 , color: this.checkInterest(item) ? 'gray':'#12AFFE' }}
                                                onPress={() => {
                                                    this.handleInterest(item)
                                                    this.setState({stopwatchStart:true})
                                                }}
                                                >{this.checkInterest(item) ? 'Interested' : 'Interest'}
                                            </Button>
                                        </View>
                                        
                                }
                                keyExtractor={(item, index) => index.toString()}
                            />)
                        }
                </View>
                <View style={{  }}>
                    <Button
                        onPress={this.goToHome}
                        title="Finish"
                        color="gray"
                        backgroundColor="red"
                        accessibilityLabel="Learn more about this purple button"

                    />
                </View>
                <FlashMessage ref="modalFlash" position="top" />
            </View>

        )
    }
}
export default ManageInterest;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 20,
    },
});