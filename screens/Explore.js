import React, { Component } from "react";
import {
    View, Image, Platform,
    Text, TouchableOpacity,
    StyleSheet, Dimensions, FlatList
} from "react-native";
import { WebView } from 'react-native-webview'
import TouchableScale from 'react-native-touchable-scale';
import Icon from 'react-native-vector-icons/Ionicons'
import { Header } from 'react-native-elements';
import * as firebase from 'firebase';
import ReadContr from './components/ReadContr';
const { height, width } = Dimensions.get('window')
import FlashMessage from "react-native-flash-message";
import SkeletonContent from "react-native-skeleton-content";

var userRef
class Explore extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            contrs: [],
            intAreas: [],
            loadingContrs: true,
            loadingInterests: true
        })
    }
    componentDidMount() {
        currentUser = firebase.auth().currentUser
        userId = firebase.auth().currentUser.uid;
        userRef = firebase.database().ref(`/users/${userId}`);
        contrRef = firebase.database().ref(`myContrs/`);
        contrRef.on('value', (childSnapshot) => {
            const contrs = [];
            childSnapshot.forEach((doc) => {
                if (userId != doc.toJSON().userId) {
                    contrs.push({
                        key: doc.key,
                        name: doc.toJSON().name,
                        image: doc.toJSON().image,
                        link: doc.toJSON().link,
                        type: doc.toJSON().type,
                        article: doc.toJSON().article,
                        userId: doc.toJSON().userId
                    });
                }
                this.setState({
                    contrs: contrs,
                    loadingContrs: false,
                });
            });
        });
        intAreaRef = userRef.child(`intArea`);
        intAreaRef.on('value', (childSnapshot) => {
            const intAreas = [];
            childSnapshot.forEach((doc) => {
                intAreas.push({
                    key: doc.key,
                    name: doc.toJSON().name,
                    newImage: doc.toJSON().newImage
                });
                this.setState({
                    intAreas: intAreas.sort((a, b) => {
                        return (a.name < b.name);
                    })
                    , loadingInterests: true
                });
            });
        });
    }
    headerLeft = () => {
        return (
            <View>
                <Text style={{ textAlign: 'center', color: 'black', fontSize: 24, fontWeight: 'bold', fontFamily: Platform.OS == 'IOS' ? 'Courier New' : null }}>Explore</Text>
            </View>
        );
    };
    checkInterest(intArea) {
        for (i = 0; i < this.state.intAreas.length; i++) {
            if (this.state.intAreas[i].name == intArea) {
                return true;
            }
        }
        return false;
    }
    addInterest = async (newIntArea) => {
        var newImage = 'https://images.unsplash.com/photo-1526655805340-274e69922288?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60';
        if (newIntArea == "Art") { newImage = 'https://images.unsplash.com/photo-1513364776144-60967b0f800f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80' }
        if (newIntArea == "Education") { newImage = 'https://images.unsplash.com/photo-1497633762265-9d179a990aa6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (newIntArea == "Science") { newImage = 'https://images.unsplash.com/photo-1507413245164-6160d8298b31?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80' }
        if (newIntArea == "Sport") { newImage = 'https://images.unsplash.com/photo-1483721310020-03333e577078?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (newIntArea == "Medicine") { newImage = 'https://images.unsplash.com/photo-1527613426441-4da17471b66d?ixlib=rb-1.2.1&auto=format&fit=crop&w=1035&q=80' }
        if (newIntArea == "Technology") { newImage = 'https://images.unsplash.com/photo-1556742521-9713bf272865?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (newIntArea == "Transportation") { newImage = 'https://images.unsplash.com/photo-1519003722824-194d4455a60c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (newIntArea == "Building") { newImage = 'https://images.unsplash.com/photo-1486406146926-c627a92ad1ab?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (newIntArea == "Self-improvement") { newImage = 'https://images.unsplash.com/photo-1508908324153-d1a219719814?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (newIntArea == "Reading") { newImage = 'https://images.unsplash.com/photo-1519682577862-22b62b24e493?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (newIntArea == "Mechanic") { newImage = 'https://images.unsplash.com/photo-1517524206127-48bbd363f3d7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (newIntArea == "Movie") { newImage = 'https://images.unsplash.com/photo-1478720568477-152d9b164e26?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (newIntArea == "Bodybuilding") { newImage = 'https://images.unsplash.com/photo-1554344728-77cf90d9ed26?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (newIntArea == "Religion") { newImage = 'https://images.unsplash.com/photo-1517244649640-fcacab5ce213?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (newIntArea == "Health") { newImage = 'https://images.unsplash.com/photo-1476480862126-209bfaa8edc8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        if (newIntArea == "Travel") { newImage = 'https://images.unsplash.com/photo-1530521954074-e64f6810b32d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }
        intAreaRef.push({
            name: newIntArea,
            newImage: newImage,
            readTime: 0
        })

    }
    openContr(name, article, type, image, userId) {
        this.refs.readContrModal.showReadContrModal(name, article, type, image, userId);
    }
    render() {
        return (
            <View style={{ }}>
                <Header
                    leftComponent={this.headerLeft}
                    leftContainerStyle={{ flex: 14 }}
                    containerStyle={{backgroundColor:'white'}}
                />
                <View style={{ paddingHorizontal: 10, flexWrap: 'wrap', justifyContent: 'space-between' }}>
                    <View>
                        {this.state.loadingContrs && this.state.loadingInterests ? (
                            <SkeletonContent
                                containerStyle={{ flex: 1, alignItems: 'center' }}
                                isLoading={this.state.loadingContrs}
                                layout={[
                                    { key: "Contrs1", width: width - 40, height: 200, marginBottom: 20 },
                                    { key: "Contrs2", width: width - 40, height: 200, marginBottom: 20 },
                                    { key: "Contrs3", width: width - 40, height: 50, marginBottom: 20 },
                                ]}
                            />) : (
                                <FlatList
                                    style={{ marginBottom: 120 }}
                                    showsVerticalScrollIndicator={false}
                                    data={this.state.contrs}
                                    numColumns={1}
                                    renderItem={({ item, key }) =>
                                        <View>
                                            <TouchableOpacity onPress={() => { this.openContr(item.name, item.article, item.type, item.image, item.userId) }}
                                                Component={TouchableScale}
                                                friction={90} //
                                                tension={100} // These props are passed to the parent component (here TouchableScale)
                                                activeScale={0.95} //
                                                style={{}}>
                                                <View style={{height: 200, borderWidth: 0.5, borderColor: '#dddddd',marginBottom:20, borderRadius:15 }}>
                                                    <View style={{ flex: 1 }}>

                                                        <Image
                                                            style={{ flex: 1, width: null, height: null, resizeMode: 'cover',borderRadius:15 }}
                                                            source={{ uri: item.image }} />

                                                    </View>
                                                    <View style={{ alignItems: 'flex-start', justifyContent: 'space-evenly', padding: 10 }}>
                                                        <Text style={{ fontSize: 12, color: 'gray' }}>{item.type}</Text>
                                                        <Text style={{ fontSize: 15, fontWeight: 'bold' }}>{item.name}</Text>
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    }

                                />)}
                    </View>
                </View>
                <FlashMessage ref="modalFlash" position="top" />
                <ReadContr ref={'readContrModal'} parentFlatList={this} ></ReadContr>
            </View>

        );
    }
}
export default Explore;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'

    },
});